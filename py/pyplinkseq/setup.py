from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext

ext_modules = [Extension(name="pyplinkseq",
                         sources=["pyplinkseq.pyx"],
                         include_dirs = ['/ddn/projects11/got2d/rivas/bin/plinkseq/include/','/ddn/projects11/got2d/rivas/py/plinkseq/py/pyplinkseqint','/ddn/projects11/got2d/rivas/rowanlib/include/'],
                         library_dirs = ['/ddn/projects11/got2d/rivas/bin/plinkseq/lib/','/ddn/projects11/got2d/rivas/py/plinkseq/py/pyplinkseqint'],
                         libraries=["pyplinkseqint"],
                         extra_compile_args=['-Wl,-no-undefined','-Wl,--warn-once'],
                         language="c++"
                         )]
setup(
  name = 'pyplinkseq',
  cmdclass = {'build_ext': build_ext},
  ext_modules = ext_modules
)
