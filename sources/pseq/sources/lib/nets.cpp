#include "plinkseq.h"
#include "netassoc.h"
#include "util.h"
#include "plinkseq/network.h"

#include <cmath>
#include <limits.h>

extern GStore g;
extern Pseq::Util::Options args;

//
// TODO
//

//  NO?    1/N connectivity weighting
//  NO?    edge permutation
//  DONE   2 seed list

//  ??     weighted nodes
//  TO COMPLETE  weighted edges
//  IN PROGRESS  influence graph enrichment analysis (Vandin et al)  HotNet

class aux_nets_t {
public:
	aux_nets_t() {
		net = NULL;

		use_background_list = false;

		use_seed_permutation = false;
		seed_perm_file = NULL;
		std::string permed_seedlist = "";

		NREP = 0;
	}

	~aux_nets_t() {
		if (net != NULL)
			delete net;

		if (seed_perm_file != NULL)
			delete seed_perm_file;
	}

	NetFunc::Network* net;

	bool use_background_list;
	std::set<std::string> background_genes;

	bool use_seed_permutation;
	InFile* seed_perm_file;
	std::string permed_seedlist;

	int NREP;
};

bool Pseq::NetFunc::setup_net(aux_nets_t& aux) {
	// attach NETDB
	if (!args.has("netdb"))
		Helper::halt("no --netdb specified");

	NetDBase netdb;

	netdb.attach(args.as_string("netdb"));

	if (!netdb.attached())
		Helper::halt("no attached NETDB");

	//
	// Options
	//
	aux.use_seed_permutation = args.has("seed-perm");

	if (aux.use_seed_permutation)
		aux.permed_seedlist = args.as_string("seed-perm");

	bool use_edge_permutation = false;
	if (args.has("edge-perm")) {
		Helper::halt("edge-permutation currently not implemented");
		use_edge_permutation = true;
	}

	// exclude list?
	aux.use_background_list = args.has("background");

	if (aux.use_background_list) {
		std::string d = args.as_string("background");
		std::string d2 = Helper::filelist2commalist(d);
		std::vector<std::string> t = Helper::parse(d2, ",");
		for (int s = 0; s < t.size(); s++)
			aux.background_genes.insert(t[s]);
		plog << "Using gene background list of " << aux.background_genes.size() << " genes\n";
	}

	bool has_exclude_from_net_list = args.has("exclude-from-net");
	std::set<std::string> exclude_from_net_list;
	if (has_exclude_from_net_list) {
		std::string d = args.as_string("exclude-from-net");
		std::string d2 = Helper::filelist2commalist(d);
		std::vector<std::string> t = Helper::parse(d2, ",");
		for (int s = 0; s < t.size(); s++)
			exclude_from_net_list.insert(t[s]);
		plog << "Using an exclude-list for the network, of " << exclude_from_net_list.size() << " genes\n";
	}

	//
	// Merge degree bins?
	//
	/* Options are:
	 * 0   : no degree matching (e.g., for general connectivity) -- permissive!
	 * 1   : complete degree matching (i.e., can have N=1 nodes in bin if node has unique degree) -- conservative!
	 * N>1 : require at least N nodes in bin -- "middle of the road"
	 */
	int merge_degree = 10; // default of "middle of the road"

	if (args.has("no-degree-matching")) {
		if (aux.use_seed_permutation)
			Helper::halt("using seed permutation, so not valid to specify degree-matching options");

		merge_degree = 0;
	}
	else if (args.has("degree-merge-bin")) {
		if (aux.use_seed_permutation)
			Helper::halt("not valid to specify bin-size for node-permutation if using seed-permutation");

		merge_degree = args.as_int("degree-merge-bin");

		if (merge_degree == 0)
			plog << "No within-degree matching for node-permutation\n";
		else if (merge_degree > 0)
			plog << "Binning degrees to have >= " << merge_degree << " nodes per bin\n";
		else
			Helper::halt("invalid degree-merge-bin");
	}

	//
	// Create network
	//
	aux.net = new ::NetFunc::Network(&netdb, args.has("edge-weights"), has_exclude_from_net_list ? &exclude_from_net_list : NULL, aux.use_background_list ? &aux.background_genes : NULL);

	aux.net->edge_permutation(use_edge_permutation);

	aux.net->min_perm_bin_size(merge_degree);

	bool bin_on_neighb_degree = false;
	if (args.has("bin-on-neighb-degree"))
		bin_on_neighb_degree = true;
	aux.net->bin_on_neighb_degree(bin_on_neighb_degree);

	::NetFunc::Network::BinOnNeighbDegreeStatistic bnds = ::NetFunc::Network::neighbSum;
	if (args.has("bin-on-neighb-degree-statistic")) {
		std::string bndsMeth = args.as_string("bin-on-neighb-degree-statistic");
		if (bndsMeth == "sum")
			bnds = ::NetFunc::Network::neighbSum;
		else if (bndsMeth == "median")
			bnds = ::NetFunc::Network::neighbMedian;
		else
			Helper::halt("Invalid bin-on-neighb-degree-statistic: " + bndsMeth);
	}
	aux.net->bin_on_neighb_degree_statistic(bnds);

	bool exclude_within_locus_edges = true;
	if (args.has("count-within-locus-edges"))
		exclude_within_locus_edges = false;
	aux.net->exclude_within_locus_edges(exclude_within_locus_edges);

	::NetFunc::Network::CrossLocusEdges cle = ::NetFunc::Network::max;
	if (args.has("cross-locus-edges")) {
		std::string cleMeth = args.as_string("cross-locus-edges");
		if (cleMeth == "sum")
			cle = ::NetFunc::Network::sum;
		else if (cleMeth == "max")
			cle = ::NetFunc::Network::max;
		else if (cleMeth == "mean")
			cle = ::NetFunc::Network::mean;
		else
			Helper::halt("Invalid cross-locus-edges: " + cleMeth);
	}
	aux.net->count_cross_locus_edges(cle);

	return true;
}

bool Pseq::NetFunc::check_node_binning(Out& pout, aux_nets_t& aux) {
	::NetFunc::Network::NodeToBinStats* sbstats = aux.net->seed_bin_stats();

	pout
	<< "#STAT" << "\t"
	<< "SEED_GENE" << "\t"
	<< "DEGREE" << "\t"
	<< "MEDIAN_NEIGHB_DEGREE" << "\t"
	<< "SUM_NEIGHB_DEGREE" << "\t"
	<< "DEGREE_BIN_RANGE" << "\t"
	<< "NUM_SEED_GENES" << "\t"
	<< "NUM_ALL_GENES" << "\t"
	<< "FRAC_SEED_GENES" << "\n";

	for (::NetFunc::Network::NodeToBinStats::iterator sbi = sbstats->begin(); sbi != sbstats->end(); ++sbi) {
		::NetFunc::Network::SingleNodeBinStats sbs = sbi->second;

		const double nseeds = sbs.numSeedNodes;
		const double ntotal = sbs.numTotalNodes;

		pout
		<< "seed_bin_stats" << "\t"
		<< sbi->first << "\t"
		<< sbs.degree << "\t" // Actual degree of gene
		<< sbs.medianNeighborDegree << "\t"
		<< sbs.sumNeighborDegree << "\t"
		<< sbs.binLabel << "\t" // Range of binned degrees
		<< nseeds << "\t" // # of seeds in this binding-degree bin
		<< ntotal << "\t" // # of genes in this binding-degree bin
		<< nseeds / ntotal << "\n"; // ratio
	}

	delete sbstats;

	return true;
}

bool Pseq::NetFunc::read_seed_groups(aux_nets_t& aux) {
	Out& seedsStatusOut = Out::stream("net.input");
	seedsStatusOut
	<< "#CLASS" << "\t"
	<< "SEED_NAME" << "\t"
	<< "INPUT_GENES" << "\t"
	<< "GENES_IN_USER_BACKGROUND" << "\t"
	<< "FINAL_SEED_GENES" << "\n";

	//
	// Seed gene list?
	//
	if (args.has("seeds"))
		read_seed_file(aux, args.as_string("seeds"), aux.net->getSeeds()->getLabel(), &::NetFunc::Network::addSeed, seedsStatusOut);

	//
	// Comparator seeds (cseeds) list?
	//
	if (args.has("cseeds"))
		read_seed_file(aux, args.as_string("cseeds"), aux.net->getCseeds()->getLabel(), &::NetFunc::Network::addCseed, seedsStatusOut);

	//
	// At this point, require some seed loci to be specified
	//
	if (!aux.use_seed_permutation) {
		if (aux.net->getSeeds()->empty())
			Helper::halt("no --seeds specified, but not using --seed-perm");
		else
			plog << aux.net->getSeeds()->numSeeds() << " seed loci in the network\n";
	}

	return true;
}

// Valid formats of input lines (tab-delimited):
#define VALID_SEED_LOCI_FORMATS \
	"\n \
	GENE(S)\n \
	LOCUS GENE(S)\n \
	LOCUS GENE(S) LOCUS-WEIGHT\n \
	\n \
	GENE(S) can be a list of genes, delimited by any of the following: ','  '+'  '/'\n \
	LOCUS-WEIGHT is assumed to be a probability p, which is converted to a -2 * ln(p) encoding"

bool Pseq::NetFunc::read_seed_file(aux_nets_t& aux, std::string seedFileName, std::string seedClassName, addGeneLocusToGroupFunc addSeedFunc, Out& pout) {
	bool added = false;
	int locIndex = 0;

	InFile seedFile(seedFileName);
	while (!seedFile.eof()) {
		std::vector<std::string> tok = seedFile.tokenizeLine("\t");
		// skip comments and empty lines
		if (tok.size() == 0)
			continue;
		if (tok[0].substr(0, 1) == "#")
			continue;

		std::string locus = "";
		std::string inputGenes;
		double weight = 1.0;

		if (tok.size() == 1) {
			// GENE(S)
			inputGenes = tok[0];
		}
		else if (tok.size() == 2) {
			// LOCUS GENE(S)
			locus = tok[0];
			inputGenes = tok[1];
		}
		else if (tok.size() == 3) {
			// LOCUS GENE(S) LOCUS-WEIGHT
			locus = tok[0];
			inputGenes = tok[1];
			weight = Helper::str2dbl(tok[2]);
		}
		else {
			stringstream str;
			str << "requires tab-delimited rows in any of the following formats: " << VALID_SEED_LOCI_FORMATS;
			Helper::halt(str.str());
		}

		/*
		 * Gives unique "numeric index" names to each Locus
		 * [thus Locus cannot be specified across multiple lines of input]:
		 */
		stringstream locusStr;
		locusStr << seedClassName << "_" << ++locIndex;
		if (locus != "")
			locusStr << ":" << locus;
		locus = locusStr.str();

		const char GENE_SEP = ',';

		std::list<std::string>* genesList = parseGenesInBackground(inputGenes, aux);
		std::stringstream genesInBackground;
		for (std::list<std::string>::const_iterator i = genesList->begin(); i != genesList->end(); ++i) {
			if (i != genesList->begin())
				genesInBackground << GENE_SEP;
			genesInBackground << *i;
		}

		const ::NetFunc::Locus* seedLoc = (aux.net->*addSeedFunc)(*genesList, weight, locus);
		std::string genesInNetworkLocus = ".";
		if (seedLoc != NULL) {
			added = true;
			genesInNetworkLocus = seedLoc->getGenesString(GENE_SEP);
		}

		pout
		<< seedClassName << "\t"
		<< locus << "\t"
		<< inputGenes << "\t"
		<< (genesInBackground.str().empty() ? "." : genesInBackground.str()) << "\t"
		<< genesInNetworkLocus << "\n";

		delete genesList;
	}
	seedFile.close();

	return added;
}

std::list<std::string>* Pseq::NetFunc::parseGenesInBackground(const std::string& genesString, aux_nets_t& aux) {
	std::list<std::string>* genesList = NULL;
	std::vector<std::string> genesVec = Helper::parse(genesString, ",+/");
	if (!aux.use_background_list) {
		genesList = new std::list<std::string>(genesVec.begin(), genesVec.end());
	}
	else { // Using a background list:
		genesList = new std::list<std::string>();
		for (std::vector<std::string>::const_iterator gIt = genesVec.begin(); gIt != genesVec.end(); ++gIt) {
			if (aux.background_genes.find(*gIt) != aux.background_genes.end())
				genesList->push_back(*gIt);
		}
	}

	return genesList;
}

bool Pseq::NetFunc::init_seed_perm_file(aux_nets_t& aux) {
	if (!aux.use_seed_permutation) {
		aux.seed_perm_file = NULL;
		return false;
	}

	if (aux.use_background_list)
		plog.warn("Using a seed permutation file, which may or may not respect the background gene list given [genes in seed loci are only retained if in background]");

	aux.seed_perm_file = new InFile(aux.permed_seedlist);
	if (aux.seed_perm_file->bad() || aux.seed_perm_file->eof())
		Helper::halt("problem opening seed-list: " + aux.permed_seedlist);

	// read original seeds
	if (!setSeedsFromNextLineInSeedPermFile(aux, 0))
		Helper::halt("no seeds read from seed-list");

	return true;
}

/* Assume FORMAT:
 * RUNTYPE P CONTRAST SAMPLE GENE(S)1 GENE(S)2 GENE(S)3...
 *
 * P==0  original data
 * P>0   all permutations (should have same # of loci as original)
 */
#define FIRST_GENE_INDEX 4

bool Pseq::NetFunc::setSeedsFromNextLineInSeedPermFile(aux_nets_t& aux, int permNum) {
	std::vector<std::string> tok = Helper::parse(aux.seed_perm_file->readLine(), "\t");
	if (tok.size() == 0)
		return false;

	if (tok.size() < (FIRST_GENE_INDEX + 1)) {
		stringstream str;
		str << "Lines in seed permutation file must contain at least one seed, and expecting " << FIRST_GENE_INDEX << " columns first";
		Helper::halt(str.str());
	}

	if (permNum != -1) {
		int pnum = Helper::str2int(tok[1]);
		if (pnum != permNum) {
			stringstream str;
			str << "In seed permutation file, expecting permutation number " << permNum;
			Helper::halt(str.str());
		}
	}
	aux.net->clearSeeds();

	for (int g = FIRST_GENE_INDEX; g < tok.size(); ++g) {
		std::string genes = tok[g];

		std::list<std::string>* genesList = parseGenesInBackground(genes, aux);
		aux.net->addSeed(*genesList);
		delete genesList;
	}

	return true;
}

bool Pseq::NetFunc::permute(aux_nets_t& aux) {
	if (aux.use_seed_permutation)
		return setSeedsFromNextLineInSeedPermFile(aux);
	else
		aux.net->permuteNodeGeneRelationships();

	return true;
}

bool Pseq::NetFunc::view_netnodes() {
	Out& pout = Out::stream("nodes");
	aux_nets_t aux;
	setup_net(aux);

	std::vector<std::string> nodes = aux.net->node_labels();
	for (int i = 0; i < nodes.size(); i++)
		pout << nodes[i] << "\n";

	return true;
}

bool Pseq::NetFunc::dump_Laplacian() {
	Out& pout = Out::stream("laplacian");
	aux_nets_t aux;
	setup_net(aux);

	Data::Matrix<double> L = aux.net->Laplacian();
	for (int r = 0; r < L.dim1(); r++) {
		for (int c = 0; c < L.dim2(); c++) {
			if (c)
				pout << "\t";
			pout << L(r, c);
		}
		pout << "\n";
	}

	return true;
}

bool Pseq::NetFunc::delta_hotnet() {
	// expecting a seed list with some large-ish number of groups,
	// that contains (e.g. 100) versions of gene scores from the null
	// distribution.   This is used to choose the value(s) of delta
	// that give the largest number of s=3,4,5 subnetworks under the
	// null (selection done manually)

	// this uses a similar code base as below.

	if (!args.has("delta-min"))
		Helper::halt("no --delta-min specified");
	if (!args.has("delta-max"))
		Helper::halt("no --delta-max specified");
	if (!args.has("delta-step"))
		Helper::halt("no --delta-step specified");

	const double delta_min = args.as_float("delta-min");
	const double delta_max = args.as_float("delta-max");
	const double delta_step = args.as_float("delta-step");

	if (!args.has("null-seeds"))
		Helper::halt("no --null-seeds specified");

	InFile seedfile(args.as_string("null-seeds"));

	std::string last_grp = "";

	bool first = true;

	std::vector<std::string> genes;

	std::vector<std::vector<double> > null_scores;
	std::vector<double>* current = NULL;

	while (!seedfile.eof()) {
		std::vector<std::string> tok = seedfile.tokenizeLine("\t");

		// skip comments and empty lines
		if (tok.size() == 0)
			continue;
		if (tok[0].substr(0, 1) == "#")
			continue;
		if (tok.size() != 3)
			Helper::halt("expecting --null-seeds file to have three columns, GROUP GENE PVALUE");

		std::string& grp = tok[0];
		std::string& gene = tok[1];
		double wgt = Helper::str2dbl(tok[2]);

		if (grp != last_grp) { // a new set of null values
			if (last_grp != "")
				first = false;
			last_grp = grp;
			std::vector<double> t;
			null_scores.push_back(t);
			current = &(null_scores[null_scores.size() - 1]);
		}

		if (first)
			genes.push_back(gene);

		// -2ln(p) encoding
		if (wgt <= 0)
			wgt = 1e-22;
		wgt = -2 * log(wgt);
		if (wgt <= 0)
			wgt = 0;

		// add value
		current->push_back(wgt);
	}
	// now, null_scores should be e.g. 100 x

	// for (int i=0; i<null_scores.size(); i++)
	//   {
	//     std::cout << i << "\t" << genes.size() << "\t" << null_scores[i].size() << "\n";
	//     for (int j = 0 ; j < 10 ; j++ ) std::cout << " " << null_scores[i][j] ;
	//     std::cout << "\n";
	//   }

	//
	// Set-up NETDB
	//
	aux_nets_t aux;
	setup_net(aux);
	::NetFunc::Network*& net = aux.net;

	//
	// Influence matrix
	//
	if (!args.has("influence-matrix"))
		Helper::halt("need to specify an influence matrix with --influence-matrix");
	if (!args.has("matdb"))
		Helper::halt("no MATDB specified");
	std::string name = args.as_string("matdb");

	MatDBase matdb;
	matdb.attach(name);
	if (!matdb.attached())
		Helper::halt("problem attaching MATDB");
	Data::Matrix<double> I = matdb.read(args.as_string("influence-matrix"));
	if (I.dim1() != I.dim2())
		Helper::halt("problem, influence matrix is not square");
	plog << "read influence matrix, for " << I.dim1() << " genes\n";

	//
	// Go through null seeds
	//
	int n_nodes, n_edges, n_nodes_in_network;
	net->basic_n(&n_nodes, &n_edges, &n_nodes_in_network);

	Out& pout = Out::stream("hotnet.delta");

	// header
	pout << "REP\tDELTA\tN_NODES\tN_EDGES\tCC";
	for (int i = 2; i <= 10; i++)
		pout << "\tN_CC_EQ" << i;
	for (int i = 2; i <= 10; i++)
		pout << "\tN_CC_GE" << i;
	pout << "\n";

	// first row: output for original data (i.e. delta = 0 )

	// get connected components
	net->calc_connected_components();
	pout << "0\t0\t" << n_nodes << "\t" << n_edges << "\t" << net->n_connected_components();

	// count connected components
	for (int s = 2; s <= 10; s++) { // of size s exactly
		std::vector<std::set<std::string> > subnets = net->get_connected_components_of_size(s);
		pout << "\t" << subnets.size();
	}
	for (int s = 2; s <= 10; s++) { // of s or more
		std::vector<std::set<std::string> > subnets = net->get_connected_components_of_size(s, 99999);
		pout << "\t" << subnets.size();
	}
	pout << "\n";

	net->clear_connected_components();

	//
	// Now consider the null replicates
	//
	for (int i = 0; i < null_scores.size(); i++) {
		plog.counter2("processing replicate " + Helper::int2str(i));

		net->clearSeeds();
		if (null_scores[i].size() != genes.size())
			Helper::halt("problem with null scores sizes");

		//
		// TODO: update HotNet functions to deal with loci of genes, instead of assuming single genes:
		//
		for (int j = 0; j < genes.size(); j++)
			net->addSeed(std::list<std::string>(1, genes[j]), null_scores[i][j]);

		for (double delta = delta_min; delta <= delta_max; delta += delta_step) {
			::NetFunc::Network* netCopy = new ::NetFunc::Network(*net);

			// get the enriched network
			netCopy->remove_edges_from_matrix_seed_score_enhanced(I, delta);

			// get connected components
			netCopy->calc_connected_components();

			// other basic stats / sanity check
			int n_nodes, n_edges, n_nodes_in_network;
			netCopy->basic_n(&n_nodes, &n_edges, &n_nodes_in_network);

			// get connected components
			pout << i << "\t" << delta << "\t" << n_nodes << "\t" << n_edges << "\t" << netCopy->n_connected_components();
			for (int s = 2; s <= 10; s++) {
				std::vector<std::set<std::string> > subnets = netCopy->get_connected_components_of_size(s);
				pout << "\t" << subnets.size();
			}

			for (int s = 2; s <= 10; s++) {
				std::vector<std::set<std::string> > subnets = netCopy->get_connected_components_of_size(s, 99999);
				pout << "\t" << subnets.size();
			}
			pout << "\n";

			delete netCopy;
		}
	}

	return true;
}

bool Pseq::NetFunc::calc_hotnet() {
	Out& pout = Out::stream("hotnet");
	aux_nets_t aux;

	//
	// Attach NETDB
	//
	setup_net(aux);

	// convenience
	::NetFunc::Network*& net = aux.net;
	::NetFunc::Network* origNet = net;

	//
	// Read a (required) weighted seed list
	//
	read_seed_groups(aux);

	//
	// Read influence matrix from a matdb
	//
	if (!args.has("influence-matrix"))
		Helper::halt("need to specify an influence matrix with --influence-matrix");

	if (!args.has("matdb"))
		Helper::halt("no MATDB specified");
	std::string name = args.as_string("matdb");

	std::cerr << "attaching influence matrix...\n";

	MatDBase matdb;
	matdb.attach(name);
	if (!matdb.attached())
		Helper::halt("problem attaching MATDB");

	Data::Matrix<double> I = matdb.read(args.as_string("influence-matrix"));

	if (I.dim1() != I.dim2())
		Helper::halt("problem, influence matrix is not square");
	plog << "read influence matrix, for " << I.dim1() << " genes\n";

	//
	// Using node permutation here: check that node-binning seems okay
	//
	check_node_binning(pout, aux);

	//
	// Pruning factor network (determined by hotnet-delta)
	//
	if (!args.has("delta"))
		Helper::halt("no --delta specified");

	const double delta = args.as_float("delta");

	//
	// Prune network
	//
	::NetFunc::Network* netCopy = new ::NetFunc::Network(*net);
	net = netCopy;
	net->remove_edges_from_matrix_seed_score_enhanced(I, delta);

	//
	// Original statistics
	//

	// output: (type of test/gene/set/value)
	::NetFunc::KeyKeyValMap output;
	::NetFunc::NameToVal orig = stat_hotnet(aux, &output);

	//
	// Clean-up
	//
	net = origNet;
	delete netCopy;

	//
	// Set up permutations
	//
	::NetFunc::NameToVal pvals;
	::NetFunc::NameToVal expected;

	aux.NREP = args.has("perm") ? args.as_int("perm") : 1000;

	for (int r = 0; r < aux.NREP; r++) {
		std::cerr << "rep = " << r << "\n";
		if (r % 10 == 0)
			plog.counter2(Helper::int2str(r) + " permutations completed");

		//
		// Permute network (using node permutation)
		//
		bool completed = !permute(aux);
		if (completed)
			break;

		//
		// Recalculate HotNet statistics
		//
		::NetFunc::Network* netCopy = new ::NetFunc::Network(*net);
		net = netCopy;
		net->remove_edges_from_matrix_seed_score_enhanced(I, delta);

		::NetFunc::NameToVal perm = stat_hotnet(aux);

		//
		// Set statistics
		//
		::NetFunc::Network::update_statistics(orig, perm, &expected, &pvals);

		//
		// Restore network
		//
		net = origNet;
		delete netCopy;
	}

	//
	// Display statistics, and p-values
	//

	//
	// (1) verbose output
	//
	for (::NetFunc::KeyKeyValMap::const_iterator oo = output.begin(); oo != output.end(); ++oo) {
		for (::NetFunc::KeyValMap::const_iterator pp = oo->second.begin(); pp != oo->second.end(); ++pp) {
			pout << oo->first << "\t" << pp->first << "\t" << pp->second << "\n";
		}
	}

	//
	// (2) network statistics
	//
	pout << "network stats\n";
	for (::NetFunc::NameToVal::KeyToVal::const_iterator kk = orig.begin(); kk != orig.end(); ++kk)
		pout << "stats_" << kk->first << "\t" << kk->second << "\t" << expected[kk->first] / (double) aux.NREP << "\t" << (pvals[kk->first] + 1.0) / (double) (aux.NREP + 1.0) << "\n";

	// // For real data, display the genes in the significant components:

	// for (int s=1;s<20;s++)
	//   {
	//     std::vector<std::set<std::string> > subnets = net->get_connected_components_of_size( s );
	//     std::cout << "---------------------------subnet of size " << s << "\n";
	//     for (int i=0;i<subnets.size();i++)
	// 	{
	// 	  std::cout << "\n";
	// 	  std::set<std::string>::iterator ii = subnets[i].begin();
	// 	  while ( ii != subnets[i].end() )
	// 	    {
	// 	      std::cout << "\t" << *ii << "\n";
	// 	      ++ii;
	// 	    }
	// 	}
	//   }

	return true;
}

::NetFunc::NameToVal Pseq::NetFunc::stat_hotnet(aux_nets_t& aux, ::NetFunc::KeyKeyValMap* out) {
	::NetFunc::NameToVal stats;

	// get connected components
	aux.net->calc_connected_components();

	const int smin = 2;
	const int smax = 50;

	bool genic_tests = true;

	std::set<std::string> added_gene;
	std::set<std::string> in_cc_gene;

	for (int s = smin; s <= smax; s++) {
		std::vector<std::set<std::string> > subnets = aux.net->get_connected_components_of_size(s, 99999);
		stats["S" + (s < 10 ? "0" + Helper::int2str(s) : Helper::int2str(s))] = subnets.size();
		std::cerr << "S" << s << "\t" << subnets.size() << "\n";
		if (out)
			(*out)["S"][Helper::int2str(s)] = Helper::int2str(subnets.size());

		for (int i = 0; i < subnets.size(); i++) {
			std::set<std::string>& subnet = subnets[i];
			std::set<std::string>::const_iterator ii = subnet.begin();
			while (ii != subnet.end()) {
				in_cc_gene.insert(*ii);
				stats["G_" + *ii] = 1;
				++ii;
			}
		}

		stats["G"] = in_cc_gene.size();

		if (out && s == 2) {
			for (int i = 0; i < subnets.size(); i++) {
				std::set<std::string>& subnet = subnets[i];
				std::cerr << "\n" << "SUBSET " << i + 1 << " S" << subnets[i].size();

				for (std::set<std::string>::const_iterator ii = subnet.begin(); ii != subnet.end(); ++ii)
					std::cerr << "\t" << *ii;
			}
		}

		//    if ( out ) (*out)[ "S" ][ Helper::int2str( s ) ] = Helper::int2str( subnets.size() );

		// if ( genic_tests )
		// 	{
		// 	  // go through all
		// 	  for (int i=0;i<subnets.size();i++)
		// 	    {
		// 	      std::set<std::string>& subnet = subnets[i];
		// 	      std::set<std::string>::const_iterator ii = subnet.begin();
		// 	      while ( ii != subnet.end() )
		// 		{
		// 		  if ( added_gene.find(*ii ) != added_gene.end() )
		// 		    {
		// 		    }

		// 		  ++ii;
		// 		}
		// 	      = aux.net->get_connected_components_of_size( s , 99999 );
		// 	}
	}

	// individual gene statistics?
	//  weight by presence in small set?
	//  let's see how many large sets are actually emerging?

	// for (int s=1;s<20;s++)
	//   {
	//     std::vector<std::set<std::string> > subnets = net->get_connected_components_of_size( s );
	//     std::cout << "---------------------------subnet of size " << s << "\n";
	//     for (int i=0;i<subnets.size();i++)
	// 	{
	// 	  std::cout << "\n";
	// 	  std::set<std::string>::iterator ii = subnets[i].begin();
	// 	  while ( ii != subnets[i].end() )
	// 	    {
	// 	      std::cout << "\t" << *ii << "\n";
	// 	      ++ii;
	// 	    }
	// 	}
	//   }

	// clean-up
	aux.net->clear_connected_components();

	return stats;
}

bool Pseq::NetFunc::calc_infl_matrix() {
	Out& pout = Out::stream("infmat");

	aux_nets_t aux;

	setup_net(aux);

	//
	// Get Laplacian
	//
	Data::Matrix<double> I = aux.net->Influence_Matrix();

	std::vector<std::string> genes = aux.net->node_labels();

	pout << genes.size();
	for (int g = 0; g < genes.size(); g++)
		pout << "\t" << genes[g];
	pout << "\n";

	for (int i = 0; i < I.dim1(); i++) {
		pout << genes[i];
		for (int j = 0; j < I.dim2(); j++)
			pout << "\t" << I(i, j);
		pout << "\n";
	}

	return true;
}


bool Pseq::NetFunc::calc_sim_matrix() 
{
  
  Out & pout = Out::stream( "simmat" );

  aux_nets_t aux;
  
  setup_net( aux );
  
  Data::Matrix<double> S = aux.net->Similarity_Matrix();
  
  std::vector<std::string> genes = aux.net->node_labels();
  
  pout << genes.size();
  for (int g = 0; g < genes.size(); g++)
    pout << "\t" << genes[g];
  pout << "\n";
  
  for (int i = 0; i < S.dim1(); i++) {
    pout << genes[i];
    for (int j = 0; j < S.dim2(); j++)
      pout << "\t" << S(i, j);
    pout << "\n";
  }
  
  return true;
}


bool Pseq::NetFunc::calc_netstats() {
	// Requires a working RNG
	CRandom::srand(time(0));

	aux_nets_t aux;

	setup_net(aux);

	::NetFunc::Network*& net = aux.net;

	//
	// Read a (required) seed list (and optionally a cseed list)
	//
	read_seed_groups(aux);

	//
	// If cseed mode, do we need to re-make the node bins?
	//
	if (net->has_cseed_list()) {
		// some auxilliary cseed options
		bool fix_cseed_genes_in_node_perms = true;
		bool include_cseed_cseed_OR_seed_seed_connections_in_cross = false;

		if (args.has("permute-cseeds"))
			fix_cseed_genes_in_node_perms = false;

		if (args.has("allow-cseed-cseed-or-seed-seed-in-cross"))
			include_cseed_cseed_OR_seed_seed_connections_in_cross = true;

		net->set_cseed_param(fix_cseed_genes_in_node_perms, include_cseed_cseed_OR_seed_seed_connections_in_cross);
	}

	//
	// Switch off various optional net-statistics
	//
	if (args.has("no-ci"))
		net->calc_common_interactors(false);
	if (args.has("no-genic"))
		net->calc_genic_scores(false);
	if (args.has("no-general"))
		net->calc_general_connectivity(false);
	if (args.has("no-indirect"))
		net->calc_indirect_connectivity(false);

	double dd1 = -1;
	plog << "mean degree distribution across network " << net->degree_distribution(&dd1, NULL) << "\n";
	plog << "mean degree distribution for in-network nodes (excluding 0-degree nodes) " << dd1 << "\n";

	//
	// If use seed permutation, open filestream and read the original seed list
	// Note: currently, this method cannot be used to specify a cseed list also
	//       can add in future easily enough I imagine
	//
	init_seed_perm_file(aux);

	//
	// If using node permutation, flag any seeds that might not get properly permuted
	// by the specified node-permutation alone (i.e. assuming no edge permutation
	// right now)
	//
	if (!aux.use_seed_permutation)
		check_node_binning(Out::stream("net.binning_stats"), aux);

	//
	// Primary outputs, original data
	//
	::NetFunc::NetDetails netDetails;
	::NetFunc::NetStatsOutput* stats = net->netstats_full(&netDetails);

	//
	// Now permute network and recalculate net-stats
	//
	::NetFunc::NetStatsOutput* pvals = new ::NetFunc::NetStatsOutput();
	::NetFunc::NetStatsOutput* expected = new ::NetFunc::NetStatsOutput();

	aux.NREP = args.has("perm") ? args.as_int("perm") : 1000;
	if (aux.use_seed_permutation)
		// so that will keep permuting until no more seed permutations left in file
		aux.NREP = INT_MAX;

	for (int r = 0; r < aux.NREP; r++) {
		if ((r+1) % 10 == 0)
			plog.counter2(Helper::int2str((r+1)) + " permutations completed");

		//
		// Permute network (using either seed or node permutation)
		//
		bool completed = !permute(aux);
		if (completed)
			break;

		//
		// Now, for this permutation, recalculate the netstats
		//
		::NetFunc::NetStatsOutput* perm = net->netstats_full();
		::NetFunc::Network::update_statistics(*stats->_summaryStats, *perm->_summaryStats, expected->_summaryStats, pvals->_summaryStats);
		::NetFunc::Network::update_statistics(*stats->_geneAndSeedStats, *perm->_geneAndSeedStats, expected->_geneAndSeedStats, pvals->_geneAndSeedStats);
		delete perm;

		// even if edge-permutation is enabled, this is probably not necessary
		//if ( use_edge_permutation ) net->restore();
	}

	//
	// All done; close the seed-permutation file if opened
	//
	if (aux.seed_perm_file) {
		aux.seed_perm_file->close();
		delete aux.seed_perm_file;
		aux.seed_perm_file = NULL;
	}

	//
	// Display statistics, and p-values
	//

	//
	// (1) Network details
	//
	Out& netDetailsOut = Out::stream("net.details");
	for (::NetFunc::NetDetails::const_iterator i = netDetails.begin(); i != netDetails.end(); ++i) {
		const ::NetFunc::NetDetailsData& ss = *i;
		for (::NetFunc::NetDetailsData::const_iterator j = ss.begin(); j != ss.end(); ++j) {
			netDetailsOut << *j << "\n";
		}
	}

	//
	// (2) General network statistics
	//
	Out& netStatsOut = Out::stream("net.stats");
	netStatsOut
	<< "#STAT" << "\t"
	<< "OBS" << "\t"
	<< "EXP" << "\t"
	<< "P" << "\n";

	for (::NetFunc::NameToVal::OrderedKeys::const_iterator kk = stats->_summaryStats->keysBegin(); kk != stats->_summaryStats->keysEnd(); ++kk) {
		const std::string& statName = *kk;
		const double& statVal = (*stats->_summaryStats)[statName];

		netStatsOut
		<< statName << "\t"
		<< statVal << "\t"
		<< (*expected->_summaryStats)[statName] / (double) aux.NREP << "\t"
		<< ((*pvals->_summaryStats)[statName] + 1.0) / (double) (aux.NREP + 1.0) << "\n";
	}

	//
	// (3) Gene- and seed-level network statistics
	//
	Out& geneSeedStatsOut = Out::stream("net.seeds_genes_stats");
	geneSeedStatsOut
	<< "#STAT" << "\t"
	<< "ELEMENT" << "\t"
	<< "OBS" << "\t"
	<< "EXP" << "\t"
	<< "P" << "\n";

	for (::NetFunc::NamePairToVal::OrderedKeys::const_iterator kk = stats->_geneAndSeedStats->keysBegin(); kk != stats->_geneAndSeedStats->keysEnd(); ++kk) {
		const ::NetFunc::StringPair& stat = *kk;
		const double& statVal = (*stats->_geneAndSeedStats)[stat];

		geneSeedStatsOut
		<< stat.first << "\t"
		<< stat.second << "\t"
		<< statVal << "\t"
		<< (*expected->_geneAndSeedStats)[stat] / (double) aux.NREP << "\t"
		<< ((*pvals->_geneAndSeedStats)[stat] + 1.0) / (double) (aux.NREP + 1.0) << "\n";
	}

	delete stats;
	delete pvals;
	delete expected;

	return true;
}
