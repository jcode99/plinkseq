#include "plinkseq/network.h"
#include "plinkseq/helper.h"
#include "plinkseq/statistics.h"
#include "plinkseq/net-connected-components.h"

#include <queue>
#include <algorithm>
#include <list>

#include <limits>

/*
 * Group
 */
const NetFunc::Locus* NetFunc::Group::addMember(const NetFunc::ConstGeneSet& genes, double weight, const std::string& locus) {
	if (genes.empty())
		return NULL;

	LocusMap::iterator findLoc = _nameToLocus.end();

	bool namedLocus = (locus != "");
	if (namedLocus) // otherwise, don't bother looking up an un-named Locus; just add a new one.
		findLoc = _nameToLocus.find(locus);

	Locus* loc = NULL;
	if (findLoc == _nameToLocus.end()) {
		// Create a new Locus:
		loc = &(*(_memberLoci->insert(_memberLoci->end(), Locus(locus, weight))));

		if (namedLocus) // don't map an un-named Locus:
			_nameToLocus[locus] = loc;
	}
	else { // locus != "", and locus already exists in _memberLoci:
		loc = findLoc->second;
		if (weight != loc->getWeight())
			// Locus already exists, but with a different weight:
			return NULL;
	}

	// Add Genes to Locus:
	for (NetFunc::ConstGeneSet::const_iterator gIt = genes.begin(); gIt != genes.end(); ++gIt) {
		const Gene* gene = *gIt;
		loc->addGene(gene);
		_geneToLoci[gene].insert(loc);
	}

	return loc;
}

/*
 * Node
 */
bool NetFunc::Node::addEdgeTo(const Node* toNode, double w) {
	_neighbors.insert(Edge(this, toNode, w)); return true;
}

bool NetFunc::Node::hasEdgeTo(const Node* toNode) const {
	return _neighbors.find(Edge(this, toNode)) != _neighbors.end();
}

bool NetFunc::Node::removeEdgeTo(const Node* toNode) {
	Neighbors::iterator findEdgeIt = _neighbors.find(Edge(this, toNode));
	if (findEdgeIt == _neighbors.end())
		return false;

	_neighbors.erase(findEdgeIt);
	return true;
}

inline unsigned int NetFunc::Node::numNeighbors() const {
	return _neighbors.size();
}

NetFunc::Node::Neighbors::const_iterator NetFunc::Node::neighbBegin() const {
	return _neighbors.begin();
}

NetFunc::Node::Neighbors::const_iterator NetFunc::Node::neighbEnd() const {
	return _neighbors.end();
}

void NetFunc::Node::removeOutgoingEdges() {
	_neighbors.clear();
}

/*
 * Net
 */
NetFunc::Network::Network(NetDBase* netdb, bool edge_weights, std::set<std::string>* excludeGenes, std::set<std::string>* onlyIncludeGenes)
: _use_edge_permutation(false), // do not use edge permutation
  _use_seed_permutation(false), // node permutation by default
  _use_edge_weights(edge_weights), // use edge-weights from NETDB
  _exclude_within_locus_edges(true),
  _fix_cseed_genes_in_node_perms(true),
  _include_cseed_cseed_OR_seed_seed_connections_in_cross(false),
  _min_perm_bin_size(10), // 'safe' degree merge threshold
  _bin_on_neighb_degree(false),
  // net-statistics to calculate
  _calc_common_interactors(true),
  _calc_genic_scores(true),
  _calc_general_connectivity(true),
  _calc_indirect_connectivity(true),
  _label2gene(), _geneToNode(), _nodes(),
  _nodePermBins(NULL),
  _components(NULL),
  _seeds(new Group("seeds")), // primary seed gene list
  _cseeds(new Group("cseeds")) { // secondary, comparator list

	_nodePermBins = new NodeBins(this);

	//
	// Load from NETDB
	//
	std::vector<int> a, b;
	std::vector<double> w; // edge weights (optional)

	//  std::cout << "using edge w " << use_edge_weights << "\n";

	std::map<int, std::string> dbnodes = netdb->fetch_all(a, b, _use_edge_weights ? &w : NULL);
	//plog << "found " << dbnodes.size() << " nodes and " << a.size() << " edges\n" ;

	//
	// construct gene/nodes
	//
	std::map<int, int> id2node;
	std::set<std::string> check_for_unique_genes;

	// start processing node list
	for (std::map<int, std::string>::const_iterator ii = dbnodes.begin(); ii != dbnodes.end(); ++ii) {
		const int geneIndInNetDB = ii->first;
		const std::string& geneName = ii->second;

		if (check_for_unique_genes.find(geneName) != check_for_unique_genes.end())
			continue;

		// implement a background list check?
		if (excludeGenes && excludeGenes->find(geneName) != excludeGenes->end())
			continue;

		// If a positive requirement list is given, and a gene is not present in this list, then exclude it:
		if (onlyIncludeGenes && onlyIncludeGenes->find(geneName) == onlyIncludeGenes->end())
			continue;

		check_for_unique_genes.insert(geneName);
		Gene* gene = new Gene(geneName);
		_label2gene[geneName] = gene;

		id2node[geneIndInNetDB] = addNode(gene);
	}

	// add edges, processing edge list.
	// if gene excluded, should return a null value from the temporary id2node, so OK.
	for (int e = 0; e < a.size(); e++) {
		std::map<int, int>::const_iterator findNode1It = id2node.find(a[e]);
		std::map<int, int>::const_iterator findNode2It = id2node.find(b[e]);

		if (findNode1It != id2node.end() && findNode2It != id2node.end()) {
			int nd1 = findNode1It->second;
			int nd2 = findNode2It->second;
			if (nd1 != nd2)
				addUndirectedEdge(nd1, nd2, _use_edge_weights ? w[e] : 1.0);

			// check that this hasn't already been added
			// if ( p1->neighbors.find(e1) != p1->neighors.end() )
			//   {
			//     std::cout << "nodes " << a[e] << " and " << b[e] << "\n";
			//     Helper::halt( "NETDB contains duplicate entries: note, assume a undirected graph. Please remake the NETDB" );
			//   }
		}
	}
}

void NetFunc::Network::copyOtherNet(const Network& net) {
	_nodePermBins = new NodeBins(this);

	_use_edge_permutation = net._use_edge_permutation;
	_use_seed_permutation = net._use_seed_permutation;

	_use_edge_weights = net._use_edge_weights;

	_exclude_within_locus_edges = net._exclude_within_locus_edges;

	_fix_cseed_genes_in_node_perms = net._fix_cseed_genes_in_node_perms;
	_include_cseed_cseed_OR_seed_seed_connections_in_cross = net._include_cseed_cseed_OR_seed_seed_connections_in_cross;

	_min_perm_bin_size = net._min_perm_bin_size;
	_bin_on_neighb_degree = net._bin_on_neighb_degree;

	_calc_common_interactors = net._calc_common_interactors;
	_calc_genic_scores = net._calc_genic_scores;
	_calc_general_connectivity = net._calc_general_connectivity;
	_calc_indirect_connectivity = net._calc_indirect_connectivity;

	// Copy the genes and nodes:
	std::map<const Gene*, const Gene*> copyToNewGene;
	for (NodeVec::const_iterator nodeIt = net._nodes.begin(); nodeIt != net._nodes.end(); ++nodeIt) {
		const Node* copyNode = *nodeIt;
		const Gene* copyGene = copyNode->getGene();

		Gene* g = new Gene(*copyGene);
		_label2gene[g->getLabel()] = g;

		addNode(g);

		copyToNewGene[copyGene] = g;
	}

	// Copy the edges:
	for (NodeVec::const_iterator nodeIt = net._nodes.begin(); nodeIt != net._nodes.end(); ++nodeIt) {
		const Node* copyNode = *nodeIt;
		for (Node::Neighbors::const_iterator neighbIt = copyNode->neighbBegin(); neighbIt != copyNode->neighbEnd(); ++neighbIt) {
			const Edge& copyEdge = *neighbIt;
			addDirectedEdge(copyEdge.getFromNode()->getNodeInd(), copyEdge.getToNode()->getNodeInd(), copyEdge.getWeight());
		}
	}

	_components = NULL;

	// Copy the group memberships:
	_seeds = copyGroup(net._seeds, copyToNewGene);
	_cseeds = copyGroup(net._cseeds, copyToNewGene);

	// Ensure that net's stats are up-to-date before copying from it:
	_degree2node = net.degree2node();

	// Calculate the degree statistics:
	_nodePermBins->calcBins();
}

NetFunc::Group* NetFunc::Network::copyGroup(const Group* group, const std::map<const Gene*, const Gene*>& copyToNewGene) {
	Group* g = new Group(group->getLabel());

	for (Group::LocusList::const_iterator membIt = group->lociBegin(); membIt != group->lociEnd(); ++membIt) {
		const Locus& copyLocus = *membIt;

		ConstGeneSet newGenes;
		for (Locus::LocusGenes::const_iterator gIt = copyLocus.genesBegin(); gIt != copyLocus.genesEnd(); ++gIt) {
			const Gene* copyGene = *gIt;
			std::map<const Gene*, const Gene*>::const_iterator findGeneIt = copyToNewGene.find(copyGene);
			if (findGeneIt != copyToNewGene.end())
				newGenes.insert(findGeneIt->second);
		}
		g->addMember(newGenes, copyLocus.getWeight(), copyLocus.getLabel());
	}

	return g;
}

NetFunc::Network::Network(const Network& net) {
	copyOtherNet(net);
}

NetFunc::Network& NetFunc::Network::operator=(const Network& net) {
	if (&net != this) {
		deleteDataMembers();
		copyOtherNet(net);
	}
	return *this;
}

void NetFunc::Network::deleteDataMembers() {
	if (_nodePermBins != NULL)
		delete _nodePermBins;

	for (LabelToGene::const_iterator geneIt = _label2gene.begin(); geneIt != _label2gene.end(); ++geneIt) {
		if (geneIt->second)
			delete geneIt->second;
	}

	for (NodeVec::const_iterator jj = _nodes.begin(); jj != _nodes.end(); ++jj) {
		if (*jj)
			delete *jj;
	}

	if (_components != NULL)
		delete _components;

	if (_seeds != NULL)
		delete _seeds;
	if (_cseeds != NULL)
		delete _cseeds;
}

NetFunc::Network::~Network() {
	deleteDataMembers();
}

int NetFunc::Network::addNode(Gene* g) {
	int ind = static_cast<int>(_nodes.size());
	Node* node = new Node(g, ind);
	_nodes.push_back(node);

	_geneToNode[g] = node;

	_nodePermBins->clearBins();

	return ind;
}

bool NetFunc::Network::addUndirectedEdge(int ind1, int ind2, double w) {
	bool add1 = addDirectedEdge(ind1, ind2, w);
	bool add2 = addDirectedEdge(ind2, ind1, w);

	return add1 && add2;
}

bool NetFunc::Network::addDirectedEdge(int fromInd, int toInd, double w) {
	_nodes[fromInd]->addEdgeTo(_nodes[toInd], w);
	_nodePermBins->clearBins();
	return true;
}

bool NetFunc::Network::removeUndirectedEdge(int ind1, int ind2) {
	bool remove1 = removeDirectedEdge(ind1, ind2);
	bool remove2 = removeDirectedEdge(ind2, ind1);

	return remove1 && remove2;
}

bool NetFunc::Network::removeDirectedEdge(int fromInd, int toInd) {
	if (_nodes[fromInd]->removeEdgeTo(_nodes[toInd])) {
		_nodePermBins->clearBins();
		return true;
	}

	return false;
}

void NetFunc::Network::removeAllEdges() {
	for (NodeVec::const_iterator ii = _nodes.begin(); ii != _nodes.end(); ++ii)
		(*ii)->removeOutgoingEdges();

	_nodePermBins->clearBins();
}

void NetFunc::Network::populate_degree2node() const {
	_degree2node.clear();

	for (GeneToNode::const_iterator ii = _geneToNode.begin(); ii != _geneToNode.end(); ++ii) {
		Node* node = ii->second;
		double degree = node->numNeighbors();
		_degree2node[degree].push_back(node);
	}
}

void NetFunc::Network::NodeBins::calcBins() {
	// Merge nodes based on degree:
	binBasedOnDegree(_net->_min_perm_bin_size);

	if (_net->_bin_on_neighb_degree) {
		NodeToQuantAttribute* aggNeighbDegrees = NULL;
		std::string aggFuncName;

		if (_net->_bin_on_neighb_degree_statistic == neighbSum) {
			aggNeighbDegrees = _net->aggregateNeighborDegrees<sumOf>();
			aggFuncName = "sum";
		}
		else if (_net->_bin_on_neighb_degree_statistic == neighbMedian) {
			aggNeighbDegrees = _net->aggregateNeighborDegrees<medianOf>();
			aggFuncName = "median";
		}

		splitNodeBins(aggNeighbDegrees, aggFuncName + "_neighb_deg", _net->_min_perm_bin_size);
	}
}

/* Bin nodes based on the following schemes:
 *
 * _min_perm_bin_size --> meaning:
 *
 *  0     -->  complete merging (all degrees to one bin, i.e. no matching on degree)
 *  1     -->  no binning
 *  N > 2 -->  ensure degree-bins have at least N nodes
 */
void NetFunc::Network::NodeBins::binBasedOnDegree(const int minBinSize) {
	// if fix_cseed_node_perms has been set to true, need to
	// take care of this (i.e. do not permute cseeds).
	// normally, this is called before the seeds/cseeds are
	// read in; in this case, this option will have no effect;
	// however, when we specify a cseed list, we will call this
	// again after reading the cseeds

	clearBins();

	ValueToNodes useDegreeToNodes;
	NodeVec cseedNodes;

	for (ValueToNodes::const_iterator ii = _net->_degree2node.begin(); ii != _net->_degree2node.end(); ++ii) {
		double degree = ii->first;
		const NodeVec& degreeNodes = ii->second;

		NodeVec curBinNodes;
		for (NodeVec::const_iterator nodeIt = degreeNodes.begin(); nodeIt != degreeNodes.end(); ++nodeIt) {
			Node* node = *nodeIt;
			const Gene* gene = node->getGene();

			if (_net->has_cseed_list() && _net->_fix_cseed_genes_in_node_perms && _net->getCseeds()->inLoci(gene))
				// will get added to a unique bin:
				cseedNodes.push_back(node);
			else
				curBinNodes.push_back(node);
		}

		useDegreeToNodes[degree] = curBinNodes;
	}

	binNodesBasedOnProperty(&useDegreeToNodes, &cseedNodes, "degree", minBinSize, _bins);
}

void NetFunc::Network::NodeBins::binNodesBasedOnProperty(const ValueToNodes* propToNodes, const NodeVec* fixedNodes, std::string nodePropName, const int minBinSize, std::list<NodeBin>* addToBins) {
	if (fixedNodes != NULL) {
		for (NodeVec::const_iterator fixedIt = fixedNodes->begin(); fixedIt != fixedNodes->end(); ++fixedIt) {
			NodeBin bin;
			bin.nodes = NodeVec(1, *fixedIt);
			bin.label = nodePropName + ":" + "fixed";
			addToBins->push_back(bin);
		}
	}

	// special case: if 0, means no property-matching, put everybody (but fixed cseeds) in a single set:
	if (minBinSize == 0) {
		NodeVec allNodes;

		for (ValueToNodes::const_iterator ii = propToNodes->begin(); ii != propToNodes->end(); ++ii) {
			double property = ii->first;
			const NodeVec& nodes = ii->second;
			allNodes.insert(allNodes.end(), nodes.begin(), nodes.end());
		}

		if (!allNodes.empty()) {
			NodeBin bin;
			bin.label = nodePropName + ":" + "no matching";
			bin.nodes = allNodes;
			addToBins->push_back(bin);
		}
		return;
	}

	/* Ensure each bin contains at least minBinSize nodes of consecutive property values:
	 *
	 * Note the special case: if minBinSize == 1, means exact property matching.
	 *
	 */
	NodeVec curBinNodes;
	double prevBinStartProp = NAN;
	double curBinStartProp = NAN;
	double curBinEndProp = NAN;

	for (ValueToNodes::const_iterator ii = propToNodes->begin(); ii != propToNodes->end(); ++ii) {
		double property = ii->first;
		const NodeVec& nodes = ii->second;

		curBinEndProp = property;
		if (isnan(curBinStartProp))
			curBinStartProp = curBinEndProp;

		curBinNodes.insert(curBinNodes.end(), nodes.begin(), nodes.end());

		if (curBinNodes.size() >= minBinSize) {
			NodeBin bin;
			bin.label = nodePropName + ":" + Helper::dbl2str(curBinStartProp) + "-" + Helper::dbl2str(curBinEndProp);
			bin.nodes = curBinNodes;
			addToBins->push_back(bin);

			prevBinStartProp = curBinStartProp;
			curBinStartProp = curBinEndProp = NAN;
			curBinNodes.clear();
		}
	}

	// Did the last set of Nodes not yet get added?
	if (!curBinNodes.empty()) {
		if (curBinNodes.size() >= minBinSize / 2.0 || isnan(prevBinStartProp))
			// We make an additional final bin if curBinNodes contains more than 50% of the required bin size (or if this will be the only [small] bin since no previous one exists...)
			addToBins->push_back(NodeBin());
		else // Otherwise, will add to previous bin:
			curBinStartProp = prevBinStartProp;

		NodeBin& lastBin = addToBins->back();
		lastBin.label = nodePropName + ":" + Helper::dbl2str(curBinStartProp) + "-" + Helper::dbl2str(curBinEndProp);
		lastBin.nodes.insert(lastBin.nodes.end(), curBinNodes.begin(), curBinNodes.end());
	}
}

void NetFunc::Network::NodeBins::splitNodeBins(NodeToQuantAttribute* nodeProperties, std::string nodePropName, const int minBinSize) {
	std::list<NodeBin>* splitBins = new std::list<NodeBin>();

	for (std::list<NodeBin>::const_iterator binIt = _bins->begin(); binIt != _bins->end(); ++binIt) {
		const NodeBin& bin = *binIt;
		const NodeVec& nodes = bin.nodes;

		ValueToNodes* usePropToNodes = new ValueToNodes();
		for (NodeVec::const_iterator nodeIt = nodes.begin(); nodeIt != nodes.end(); ++nodeIt) {
			Node* nd = *nodeIt;
			NodeToQuantAttribute::const_iterator findNodeProp = nodeProperties->find(nd);
			if (findNodeProp == nodeProperties->end())
				Helper::halt("Missing " + nodePropName + " property for Node " + nd->getGene()->getLabel());
			(*usePropToNodes)[findNodeProp->second].push_back(nd);
		}

		std::string useNodePropName = bin.label + ";" + nodePropName;

		// NULL ==> no fixed matching nodes:
		binNodesBasedOnProperty(usePropToNodes, NULL, useNodePropName, minBinSize, splitBins);

		delete usePropToNodes;
	}

	delete _bins;
	_bins = splitBins;

	delete nodeProperties;
}

void NetFunc::Network::basic_n(int* n_nodes, int* n_edges, int* n_nodes_in_network) const {
	*n_nodes = _nodes.size();
	int e = 0;
	int n1 = 0; // nodes w/ 1 or more edge

	for (NodeVec::const_iterator ii = _nodes.begin(); ii != _nodes.end(); ++ii) {
		const Node* nd = *ii;
		e += nd->numNeighbors();
		if (nd->numNeighbors() > 0)
			++n1;
	}

	// For an undirected graph, e should be an even #
	*n_edges = int(e / 2.0);
	*n_nodes_in_network = n1;
}

double NetFunc::Network::degree_distribution(double* x0, std::map<int, int>* dd) {
	// calculate mean degree
	int numer = 0;
	int denom = 0;
	int denomx0 = 0; // do not include 0-degree nodes in the mean (returned in x0)

	if (dd)
		dd->clear();

	for (ValueToNodes::const_iterator jj = degree2node().begin(); jj != degree2node().end(); ++jj) {
		int degree = jj->first;
		int numWithDegree = jj->second.size();

		numer += degree * numWithDegree;
		denom += numWithDegree;
		if (degree > 0)
			denomx0 += numWithDegree;

		if (dd)
			(*dd)[degree] = numWithDegree;
	}

	*x0 = denomx0 == 0 ? 0.0 : (double) numer / (double) denomx0;

	return denom == 0 ? 0.0 : (double) numer / (double) denom;
}

// permute network (in-degree, followed by edge permutation)
void NetFunc::Network::permuteNodeGeneRelationships() {
	// node permutation, but stratified by 'merged_degree2node' groupings
	// optional edge permutation, for otherwise un-permuted nodes

	// if ( use_edge_permutation )
	//   edge_permutation_originals.clear();

	for (std::list<NodeBin>::const_iterator jj = _nodePermBins->bins().begin(); jj != _nodePermBins->bins().end(); ++jj) {
		const NodeVec& d = jj->nodes;
		const int n = d.size();

		// no point in permuting these
		if (n == 1) {
			// requires edge permutation?
			// if ( use_edge_permutation )
			//   edge_permutation_originals[ d[0] ] = (*d[0]).conn;
			continue;
		}

		// shuffle
		std::vector<int> a(n, 0);
		random_draw(a);

		// Store the original genes list
		std::vector<Gene*> o(n);
		for (int i = 0; i < n; i++)
			o[i] = d[i]->getGene();

		// Swap the Node -> Gene, Gene -> Node mappings:
		for (int i = 0; i < n; i++) {
			// swap gene onto a different node
			d[i]->setGene(o[a[i]]);
			_geneToNode[d[i]->getGene()] = d[i];
		}
	}

	// determine whether we need to do edge permutation...

	// if ( false use_edge_permutation )
	//   {

	//     const int ne = edge_permutation_originals.size();
	//     int ce = 0;
	//     std::map<Node*,std::set<Connection> >::const_iterator ee = edge_permutation_originals.begin();
	//     while ( ee != edge_permutation_originals.end() )
	// 	{
	// 	  bool permuted = false;
	// 	  while ( ! permuted )
	// 	    {
	// 	      // choose a conncetion
	// 	      int r = CRandom::rand( ne );
	// 	      // no self-self
	// 	      if ( ne == ce ) continue;

	// 	    }

	// 	  ++ee;
	// 	}
	//   }
}

Data::Matrix<double> NetFunc::Network::Adjacency() const {
	const int n = _nodes.size();
	Data::Matrix<double> A(n, n, 0);

	for (NodeVec::const_iterator ii = _nodes.begin(); ii != _nodes.end(); ++ii) {
		const Node* nd1 = *ii;
		for (Node::Neighbors::const_iterator jj = nd1->neighbBegin(); jj != nd1->neighbEnd(); ++jj) {
			const Node* nd2 = jj->getToNode();
			A[nd1->getNodeInd()][nd2->getNodeInd()] = 1;
		}
	}

	return A;
}

Data::Matrix<double> NetFunc::Network::Similarity_Matrix() const 
{
  
  const int n = _nodes.size();
  
  Data::Matrix<double> S(n, n, 0);
  
  for (NodeVec::const_iterator ii = _nodes.begin(); ii != _nodes.end(); ++ii) 
    {
      const Node * node = *ii;
      const int    idx1 = node->getNodeInd();
      
      std::cerr << "processing " << idx1 << "\t" << node->getGene()->getLabel() << "\n";

      std::map<NetFunc::Node*,double> d = shortestPathLengths( node );
      
      for (NodeVec::const_iterator jj = _nodes.begin(); jj != _nodes.end(); ++jj) 
	{ 
	  
	  Node * node2 = *jj;
	  
	  double distance = d[ node2 ];
	  
	  const int idx2 = node2->getNodeInd();

	  // std::cout << "D\t"
	  // 	    << idx1 << "-" << idx2 << "\t"
	  // 	    << node->getGene()->getLabel() << "\t"
	  // 	    << node2->getGene()->getLabel() << "\t";
	  
	  // 'similarity' is 1/d

	  if ( distance == std::numeric_limits<double>::infinity() )
	    S( idx1 , idx2 ) = 0;
	  else
	    S( idx1, idx2 ) = 1.0 / distance;
	  
	  if ( idx1 == idx2 ) 
	    S( idx1 , idx2 ) = 1.0;

	}      
      
    }      
  
  return S;
}

	    // struct paths_t 
	    // {
	    //   int size() const { return paths.size(); }
	    //   std::vector<std::vector<Node*> > paths;
	    //   std::vector<double> wgts;
	    // };
	  

      
//       const int n = _nodes.size();
      
//       for (int i = 0; i < n; i++) 
// 	{
	  
// 	  std::vector<NetFunc::Network::ConstNodeList> paths = DijkstraGetShortestPaths(_nodes[i], &previous);
	  
// 	  std::cout << "distance from " << a->getGene()->getLabel() << " to " << _nodes[i]->getGene()->getLabel() << "\t" << min_distance[i] << "\n";
	  
// 	  for (int p = 0; p < paths.size(); p++) 
// 	    {
// 	      std::cout << "\tpath" << p << "\t";
	      
// 	      for (ConstNodeList::const_iterator ii = paths[p].begin(); ii != paths[p].end(); ++ii)
// 		std::cout << " " << (*ii)->getGene()->getLabel();
	      
// 	      std::cout << "\n";
// 	    }
// 	}
           
//     }
// }

Data::Matrix<double> NetFunc::Network::Laplacian() const {
	// degree matrix - adjacency matrix
	// http://en.wikipedia.org/wiki/Laplacian_matrix

	const int n = _nodes.size();
	Data::Matrix<double> L(n, n, 0);

	for (NodeVec::const_iterator ii = _nodes.begin(); ii != _nodes.end(); ++ii) {
		const Node* nd1 = *ii;
		for (Node::Neighbors::const_iterator jj = nd1->neighbBegin(); jj != nd1->neighbEnd(); ++jj) {
			const Node* nd2 = jj->getToNode();
			L[nd1->getNodeInd()][nd2->getNodeInd()] = -1;
		}

		// degree on diagonal
		L[nd1->getNodeInd()][nd1->getNodeInd()] = nd1->numNeighbors();
	}

	return L;
}

Data::Matrix<double> NetFunc::Network::Normalized_Laplacian() const {
	// http://www.math.ucsd.edu/~fan/research/cb/ch1.pdf

	const int n = _nodes.size();
	Data::Matrix<double> L(n, n, 0);

	for (NodeVec::const_iterator ii = _nodes.begin(); ii != _nodes.end(); ++ii) {
		const Node* nd1 = *ii;
		int degree = nd1->numNeighbors();

		for (Node::Neighbors::const_iterator jj = nd1->neighbBegin(); jj != nd1->neighbEnd(); ++jj) {
			const Node* nd2 = jj->getToNode();
			L[nd1->getNodeInd()][jj->getToNode()->getNodeInd()] = -1.0 / sqrt(degree * jj->getToNode()->numNeighbors());
		}

		// degree on diagonal
		L[nd1->getNodeInd()][nd1->getNodeInd()] = degree ? 1 : 0;
	}

	return L;
}

NetFunc::ConstGeneSet NetFunc::Network::find_genes(const std::list<std::string>& labels) const {
	ConstGeneSet genes;

	for (std::list<std::string>::const_iterator i = labels.begin(); i != labels.end(); ++i) {
		LabelToGene::const_iterator findLabel = _label2gene.find(*i);
		if (findLabel != _label2gene.end())
			genes.insert(findLabel->second);
	}

	return genes;
}

const NetFunc::Locus* NetFunc::Network::addSeed(const std::list<std::string>& genes, double weight, const std::string& locus) {
	return _seeds->addMember(find_genes(genes), weight, locus);
}

void NetFunc::Network::clearSeeds() {
	_seeds->clear();
}

// ---- Comparator cseeds ------
const NetFunc::Locus* NetFunc::Network::addCseed(const std::list<std::string>& genes, double weight, const std::string& locus) {
	const NetFunc::Locus* loc = _cseeds->addMember(find_genes(genes), weight, locus);

	if (loc != NULL)
		// Since may want (or not want) to permute cSeeds, changing them require a rebinning of nodes to permute:
		_nodePermBins->clearBins();

	return loc;
}

void NetFunc::Network::clearCseeds() {
	_cseeds->clear();

	// Since may want (or not want) to permute cSeeds, changing them require a rebinning of nodes to permute:
	_nodePermBins->clearBins();
}

// ---- Partner lookup for a gene ------------
std::map<std::string, int> NetFunc::Network::connections(const std::string& label, const int depth) const {
	std::map<std::string, int> s;

	Gene* gene = find_gene(label);
	if (gene == NULL)
		return s;

	std::cout << "found a non-null GENE\n";
	s[gene->getLabel()] = 0;

	Node* keynode = _geneToNode.find(gene)->second;

	std::map<const Node*, int> p = partners(keynode, depth);

	for (std::map<const Node*, int>::const_iterator ii = p.begin(); ii != p.end(); ++ii)
		s[ii->first->getGene()->getLabel()] = ii->second;

	return s;
}

std::map<const NetFunc::Node*, int> NetFunc::Network::partners(const Node* root, const int depth) const {
	std::map<const Node*, int> p;
	std::queue<const Node*> q;

	q.push(root);
	p[root] = 0;

	while (true) {
		if (q.size() == 0)
			break;
		const Node* u = q.front();
		q.pop();
		if (p[u] == depth)
			continue;

		for (Node::Neighbors::const_iterator ii = u->neighbBegin(); ii != u->neighbEnd(); ++ii) {
			const Node* v = ii->getToNode();
			if (p.find(v) == p.end()) {
				q.push(v);
				p[v] = p[u] + 1;
			}
		}
	}

	return p;
}

int NetFunc::Network::degree(const std::string& label) const {
	Gene* gene = find_gene(label);
	if (gene == NULL)
		return 0;

	const Node* node = _geneToNode.find(gene)->second;
	return node->numNeighbors();
}

/*
 * Have a directed Edge: node1 -> node2 (gene1 -> gene2). gene1 is in Locus loc1, gene2 in Locus loc2.
 *
 * NOTE: In the following code, the PER GENE scores include all (non-intra-locus) edges,
 * whereas the PER SEED scores ensure that multiple cross-loci edges are counted as specified by count_cross_locus_edges().
 */
void NetFunc::Network::updateNetStats(const Node* node1, const Node* node2, const Locus* loc1, const Locus* loc2, double wgt, LocToLocStatsAggregator& locEdgesAgg, GeneToLocPairStatsAggregator& geneEdgesAgg, Edge* edg, BasicNetStats* storeDetails) const {
	const Gene* gene1 = node1->getGene();
	const Gene* gene2 = node2->getGene();

	LocPair lp(loc1, loc2);

	// PER SEED: for loc1, find its aggregator, and update that another loc1 -> loc2 Edge was observed:
	LocStatisticAggregator* loc1Agg = ensureAggregator(loc1, locEdgesAgg);
	loc1Agg->addStat(loc2, wgt);

	// PER GENE: for gene1, find its aggregator, and update that another loc1 -> loc2 Edge was observed:
	LocPairStatisticAggregator* gene1Agg = ensureAggregator(gene1, geneEdgesAgg);
	gene1Agg->addStat(lp, wgt);

	if (storeDetails != NULL) {
		if (edg != NULL) {
			storeDetails->seed_edges[lp].insert(edg);
			storeDetails->edges.insert(edg);
		}
		storeDetails->genes.insert(gene1);
		storeDetails->genes.insert(gene2);
	}
}

//
// return network statistics
//
void NetFunc::Network::netstats_singleSeedGroup
		(const Group* seeds, // input
		NetDetails* netDetails, // verbose output
		SingleSeedGroupStats* groupStats) const {

	// TODO can weight this statistic by gene/loci weights (ASSUMING WE WANT TO IMPLEMENT SUCH A STATISTIC):
	groupStats->n_seeds = seeds->numSeeds();
	groupStats->n_seed_genes = seeds->numSeedGenes();

	Group* genesAsSingletons = new Group("dummy_singleton_gene_loci");

	LocToLocStatsAggregator generalLocEdgesAggregator;
	LocToLocStatsAggregator directLocEdgesAggregator;
	LocToLocStatsAggregator indirectLocEdgesAggregator;

	GeneToLocPairStatsAggregator generalGeneEdgesAggregator;
	GeneToLocPairStatsAggregator directGeneEdgesAggregator;
	GeneToLocPairStatsAggregator indirectGeneEdgesAggregator;

	GeneToLocPairStatsAggregator CIagg;

	NodePairToEdge* directEdges = NULL;
	NodePairToIndirectEdge* indirectEdges = NULL;
	if (netDetails != NULL) {
		directEdges = new NodePairToEdge();
		indirectEdges = new NodePairToIndirectEdge();
	}

	//EdgeType* ensureEdge(const Node* node1, const Node* node2, double wgt, NodePairToEdge& edgeMap)

	for (Group::GeneToLoci::const_iterator seedGeneIt = seeds->genesBegin();
			seedGeneIt != seeds->genesEnd(); ++seedGeneIt) {
		const Gene* seedGene = seedGeneIt->first;
		const ConstLocusSet* seedGeneLoci = &(seedGeneIt->second);

		/*
		// Ensure that seedGene is registered for these stats, even if none (ensures they will all be output):
		// PROBLEM WITH UNCOMMENTING OUT THIS CODE: WOULD NOT ALLOW US TO USE THIS TO COUNT HOW MANY GENES HAVE AT LEAST ONE CONNECTION...
		(groupStats->gene_n_general_connections)[seedGene] = 0;
		(groupStats->gene_n_direct_connections)[seedGene] = 0;
		(groupStats->gene_n_indirect_connections)[seedGene] = 0;
		*/

		// get Node that corresponds to this Gene:
		const Node* seedNode = _geneToNode.find(const_cast<Gene*>(seedGene))->second;

		for (Node::Neighbors::const_iterator neighbIt = seedNode->neighbBegin(); neighbIt != seedNode->neighbEnd(); ++neighbIt) {
			const Edge& neighbEdge = *neighbIt;
			const Node* neighb = neighbEdge.getToNode();
			const Gene* neighbGene = neighb->getGene();
			const ConstLocusSet* neighbGeneSeedLoci = seeds->getLoci(neighbGene);

			const double wgt = neighbEdge.getWeight() * (seedGene->getWeight() * neighbGene->getWeight());

			const ConstLocusSet* neighbGeneAsSingletonLocus = genesAsSingletons->getLoci(neighbGene);
			if (neighbGeneAsSingletonLocus == NULL) {
				ConstGeneSet g;
				g.insert(neighbGene);
				genesAsSingletons->addMember(g);
				neighbGeneAsSingletonLocus = genesAsSingletons->getLoci(neighbGene);
			}
			if (neighbGeneAsSingletonLocus->size() != 1)
				Helper::halt("Error: neighbor gene as singleton should only have one Locus");
			const Locus* neighbGeneAsSingleton = *(neighbGeneAsSingletonLocus->begin());

			for (ConstLocusSet::const_iterator i = seedGeneLoci->begin(); i != seedGeneLoci->end(); ++i) {
				const Locus* loc1 = *i;
				double useLoc1Wgt = wgt * loc1->getWeight();
				/* General network seed connectivity */
				updateNetStats(seedNode, neighb, loc1, neighbGeneAsSingleton, useLoc1Wgt, generalLocEdgesAggregator, generalGeneEdgesAggregator);

				if (neighbGeneSeedLoci != NULL) { // neighb is also a seed
					for (ConstLocusSet::const_iterator j = neighbGeneSeedLoci->begin(); j != neighbGeneSeedLoci->end(); ++j) {
						const Locus* loc2 = *j;
						if (loc1 != loc2 || !exclude_within_locus_edges()) {
							double useWgt = useLoc1Wgt * loc2->getWeight();
							/* Direct (seed-seed) connectivity */
							Edge* e = ensureEdge<Edge>(seedNode, neighb, useWgt, directEdges);
							updateNetStats(seedNode, neighb, loc1, loc2, useWgt, directLocEdgesAggregator, directGeneEdgesAggregator, e, &groupStats->direct);
						}
					}
				}
			}

			// Find all indirect interactors, stats on the common interactors (CI) themselves:
			if (_calc_indirect_connectivity || _calc_common_interactors) {
				for (Node::Neighbors::const_iterator neighbNeighbIt = neighb->neighbBegin(); neighbNeighbIt != neighb->neighbEnd(); ++neighbNeighbIt) {
					const Edge& neighbNeighbEdge = *neighbNeighbIt;
					const Node* neighbNeighb = neighbNeighbEdge.getToNode();
					if (neighbNeighb == seedNode) // don't count when seedNode is "indirectly connected" to itself via neighb
						continue;
					const Gene* neighbNeighbGene = neighbNeighb->getGene();
					const ConstLocusSet* neighbNeighbGeneSeedLoci = seeds->getLoci(neighbNeighbGene);

					const double neighbNeighbWgt = neighbNeighbEdge.getWeight() * (neighbNeighbGene->getWeight());

					if (neighbNeighbGeneSeedLoci != NULL) { // found an indirect seed-seed interaction (via neighb)
						// ASSUME that Edges are multiplicative
						double indirWgt = wgt * neighbNeighbWgt;

						for (ConstLocusSet::const_iterator i = seedGeneLoci->begin(); i != seedGeneLoci->end(); ++i) {
							const Locus* loc1 = *i;
							for (ConstLocusSet::const_iterator j = neighbNeighbGeneSeedLoci->begin(); j != neighbNeighbGeneSeedLoci->end(); ++j) {
								const Locus* loc2 = *j;
								if (loc1 != loc2 || !exclude_within_locus_edges()) {
									double useIndirWgt = indirWgt * (loc1->getWeight() * loc2->getWeight());
									/* Indirect (seed-seed) connectivity */

									/* NOTE: In the simplest case, indirect.gene_n_connections tallies total # of
									 * indirect connections == number of paths such that: seedGene <---> CI <---> otherSeed [summed over all other seeds]
									 */
									IndirectEdge* e = ensureEdge<IndirectEdge>(seedNode, neighbNeighb, useIndirWgt, indirectEdges);
									if (e != NULL) e->addIntermediate(neighb);
									updateNetStats(seedNode, neighbNeighb, loc1, loc2, useIndirWgt, indirectLocEdgesAggregator, indirectGeneEdgesAggregator, e, &groupStats->indirect);

									/* CI connectivity */
									// PER CI GENE
									LocPairStatisticAggregator* neighbGeneAgg = ensureAggregator(neighbGene, CIagg);
									neighbGeneAgg->addStat(LocPair(loc1,loc2), useIndirWgt);
								}
							}
						}
					}
				}
			}

			// interactors is a map of: gene -> set of seed genes it interacts with
			if (_calc_common_interactors)
				(groupStats->interactors)[neighbGene].insert(seedGene);
		}
	}

	groupStats->general.agg_seed_n_connections = sumAndDeleteAggregators(generalLocEdgesAggregator, groupStats->general.seed_n_connections);
	groupStats->direct.agg_seed_n_connections = sumAndDeleteAggregators(directLocEdgesAggregator, groupStats->direct.seed_n_connections);
	groupStats->indirect.agg_seed_n_connections = sumAndDeleteAggregators(indirectLocEdgesAggregator, groupStats->indirect.seed_n_connections);

	groupStats->general.agg_gene_n_connections = sumAndDeleteAggregators(generalGeneEdgesAggregator, groupStats->general.gene_n_connections);
	groupStats->direct.agg_gene_n_connections = sumAndDeleteAggregators(directGeneEdgesAggregator, groupStats->direct.gene_n_connections);
	groupStats->indirect.agg_gene_n_connections = sumAndDeleteAggregators(indirectGeneEdgesAggregator, groupStats->indirect.gene_n_connections);

	sumAndDeleteAggregators(CIagg, groupStats->gene_n_interactor_count);

	delete genesAsSingletons;

	/*
	// Stats on the common interactors (CI) themselves:
	for (GeneToGenes::const_iterator ciIt = groupStats->interactors.begin(); ciIt != groupStats->interactors.end(); ++ciIt) {
		const Gene* CI = ciIt->first;
		const ConstGeneSet& seedInteractors = ciIt->second;

		const int sz = seedInteractors.size();
		if (sz >= 2) {
			(groupStats->gene_n_interactor_count)[CI] = sz;
			groupStats->agg_seed_n_interactor_count += sz;
		}
	}
	*/

	if (netDetails) {
		std::string seedPrefix = seeds->getLabel() + "_";

		list<const Group*> seedsList(1, seeds);
		printEdges(groupStats->direct, netDetails, seedsList, seedPrefix, true);
		if (_calc_indirect_connectivity)
			printEdges(groupStats->indirect, netDetails, seedsList, seedPrefix, true);

		// Now, the common interactors (CI):
		// RECALL THAT: interactors = a map of: gene -> set of seed genes it interacts with
		StringSet ci;
		for (GeneToGenes::const_iterator ciIt = groupStats->interactors.begin(); ciIt != groupStats->interactors.end(); ++ciIt) {
			const Gene* CI = ciIt->first;
			const ConstGeneSet& seedInteractors = ciIt->second;

			const int sz = seedInteractors.size();
			if (sz >= 2) { // CI connects 2 or more seeds
				std::stringstream ss;
				ss
				<< seedPrefix + "common-interactor" << "\t"
				<< CI->getLabel() << "\t"
				<< sz << "\t";

				for (ConstGeneSet::const_iterator gg = seedInteractors.begin(); gg != seedInteractors.end(); ++gg) {
					if (gg != seedInteractors.begin())
						ss << "\t";
					ss << (*gg)->getLabel();
				}
				ci.insert(ss.str());
			}
		}
		netDetails->push_back(NetDetailsData(ci.begin(), ci.end()));
	}

	deleteEdgeMap(directEdges);
	deleteEdgeMap(indirectEdges);
}

void NetFunc::Network::printEdges(const BasicNetStats& netStats, NetDetails* netDetails, list<const Group*>& seeds, const std::string& prefix, bool printABiffAlessThanB) {
	std::string usePrefix = prefix + netStats.name() + "_";

	std::map<const Locus*, int> seedToIndex;
	int ind = 0;
	for (list<const Group*>::const_iterator i = seeds.begin(); i != seeds.end(); ++i) {
		const Group* seedLoci = *i;
		for (Group::LocusList::const_iterator j = seedLoci->lociBegin(); j != seedLoci->lociEnd(); ++j)
			seedToIndex[&(*j)] = ++ind;
	}

	typedef std::pair<int, int> IndexPair;
	typedef std::pair<const LocPair*, const ConstEdgeSet*> LocEdgesElem;
	std::map<IndexPair, LocEdgesElem*> seed_edges;
	for (LocEdges::const_iterator led = netStats.seed_edges.begin(); led != netStats.seed_edges.end(); ++led) {
		const LocPair& lp = led->first;
		IndexPair ip(seedToIndex[lp.first], seedToIndex[lp.second]);
		seed_edges[ip] = new LocEdgesElem(&led->first, &led->second);
	}

	NetDetailsData se;
	for (std::map<IndexPair, LocEdgesElem*>::const_iterator led = seed_edges.begin(); led != seed_edges.end(); ++led) {
		const IndexPair& ip = led->first;
		const LocEdgesElem* it = led->second;
		const LocPair& lp = *(it->first);

		// TODO Remove the following condition if we want to simply handle directed networks:
		if (!printABiffAlessThanB || ip.first < ip.second) {
			const ConstEdgeSet& componentEdges = *(it->second);

			stringstream ss;
			ss
			<< usePrefix + "seed_edge" << "\t"
			<< lp.first->toString() + " <--> " + lp.second->toString();

			StringSet sSet;
			for (ConstEdgeSet::const_iterator edg = componentEdges.begin(); edg != componentEdges.end(); ++edg)
				sSet.insert((*edg)->toString());
			for (StringSet::const_iterator i = sSet.begin(); i != sSet.end(); ++i)
				ss << "\t" << *i;

			se.push_back(ss.str());
		}
		delete it;
	}
	netDetails->push_back(se);

	StringSet e;
	for (ConstEdgeSet::const_iterator ed = netStats.edges.begin(); ed != netStats.edges.end(); ++ed) {
		const Gene* fromGene = (*ed)->getFromNode()->getGene();
		const Gene* toGene = (*ed)->getToNode()->getGene();

		// TODO Remove the following condition if we want to simply handle directed networks:
		if (!printABiffAlessThanB || (fromGene->getLabel() < toGene->getLabel())) {
			stringstream ss;
			ss
			<< usePrefix + "gene_edge" << "\t"
			<< (*ed)->toString();
			e.insert(ss.str());
		}
	}
	netDetails->push_back(NetDetailsData(e.begin(), e.end()));

	StringSet g;
	for (ConstGeneSet::const_iterator gIt = netStats.genes.begin(); gIt != netStats.genes.end(); ++gIt) {
		stringstream ss;
		ss
		<< usePrefix + "gene" << "\t"
		<< (*gIt)->getLabel();
		g.insert(ss.str());
	}
	netDetails->push_back(NetDetailsData(g.begin(), g.end()));

}

void NetFunc::Network::netstats_crossSeedGroups(const Group* seeds1, const Group* seeds2, NetDetails* netDetails, CrossSeedGroupStats* crossStats) const {
	LocToLocStatsAggregator selfCrossLocEdgesAggregator;
	LocToLocStatsAggregator directCrossLocEdgesAggregator;
	LocToLocStatsAggregator indirectCrossLocEdgesAggregator;

	GeneToLocPairStatsAggregator selfCrossGeneEdgesAggregator;
	GeneToLocPairStatsAggregator directCrossGeneEdgesAggregator;
	GeneToLocPairStatsAggregator indirectCrossGeneEdgesAggregator;

	GeneToLocPairStatsAggregator CIagg;

	NodePairToEdge* selfEdges = NULL;
	NodePairToEdge* directEdges = NULL;
	NodePairToIndirectEdge* indirectEdges = NULL;
	if (netDetails != NULL) {
		selfEdges = new NodePairToEdge();
		directEdges = new NodePairToEdge();
		indirectEdges = new NodePairToIndirectEdge();
	}

	/* Direct (seeds1-seeds2) cross-connectivity */
	for (Group::GeneToLoci::const_iterator s1It = seeds1->genesBegin(); s1It != seeds1->genesEnd(); ++s1It) {
		const Gene* seed1Gene = s1It->first;
		const ConstLocusSet* seed1GeneSeeds1Loci = &(s1It->second);

		// get Node that corresponds to this Gene:
		const Node* seed1GeneNode = _geneToNode.find(const_cast<Gene*>(seed1Gene))->second;

		// a "self-connection":
		const ConstLocusSet* seed1GeneSeeds2Loci = seeds2->getLoci(seed1Gene);
		if (seed1GeneSeeds2Loci != NULL) {
			const double wgt = 1.0 * seed1Gene->getWeight();

			for (ConstLocusSet::const_iterator i = seed1GeneSeeds1Loci->begin(); i != seed1GeneSeeds1Loci->end(); ++i) {
				const Locus* loc1 = *i;
				for (ConstLocusSet::const_iterator j = seed1GeneSeeds2Loci->begin(); j != seed1GeneSeeds2Loci->end(); ++j) {
					const Locus* loc2 = *j;
					double useWgt = wgt * (loc1->getWeight() * loc2->getWeight());
					/* "Self" (seeds1-seeds2) cross-connectivity */
					Edge* e = ensureEdge<Edge>(seed1GeneNode, seed1GeneNode, useWgt, selfEdges);
					updateNetStats(seed1GeneNode, seed1GeneNode, loc1, loc2, useWgt, selfCrossLocEdgesAggregator, selfCrossGeneEdgesAggregator, e, &crossStats->self_cross);
				}
			}
		}

		for (Node::Neighbors::const_iterator neighbIt = seed1GeneNode->neighbBegin(); neighbIt != seed1GeneNode->neighbEnd(); ++neighbIt) {
			const Edge& neighbEdge = *neighbIt;
			const Node* neighb = neighbEdge.getToNode();
			const Gene* neighbGene = neighb->getGene();
			const ConstLocusSet* neighbGeneSeeds2Loci = seeds2->getLoci(neighbGene);

			const double wgt = neighbEdge.getWeight() * (seed1Gene->getWeight() * neighbGene->getWeight());

			if (neighbGeneSeeds2Loci != NULL && // neighbGene is in seeds2, so found a cross-edge: geneNode (in seeds1) -> neighb (in seeds2)
					(_include_cseed_cseed_OR_seed_seed_connections_in_cross || ((!seeds2->inLoci(seed1Gene)) && (!seeds1->inLoci(neighbGene))))) { // option --allow-cseed-cseed-in-cross relaxes this constraint
				for (ConstLocusSet::const_iterator i = seed1GeneSeeds1Loci->begin(); i != seed1GeneSeeds1Loci->end(); ++i) {
					const Locus* loc1 = *i;
					for (ConstLocusSet::const_iterator j = neighbGeneSeeds2Loci->begin(); j != neighbGeneSeeds2Loci->end(); ++j) {
						const Locus* loc2 = *j;
						double useWgt = wgt * (loc1->getWeight() * loc2->getWeight());
						/* Direct (seeds1-seeds2) cross-connectivity */
						Edge* e = ensureEdge<Edge>(seed1GeneNode, neighb, useWgt, directEdges);
						updateNetStats(seed1GeneNode, neighb, loc1, loc2, useWgt, directCrossLocEdgesAggregator, directCrossGeneEdgesAggregator, e, &crossStats->direct_cross);
					}
				}
			}

			if (_calc_indirect_connectivity || _calc_common_interactors) {
				/* Want to consider a common interactor between seeds1 and seeds2:
				 * seed1 <---> neighb <---> seed2
				 * where neighb itself is not a seed1 or a seed2.
				 *
				 * The following condition (among other things) makes a self-self interaction impossible in the above chain, since neighb won't be a seed1 or a seed2:
				 */
				if (seeds1->inLoci(neighbGene) || seeds2->inLoci(neighbGene))
					continue;

				for (Node::Neighbors::const_iterator neighbNeighbIt = neighb->neighbBegin(); neighbNeighbIt != neighb->neighbEnd(); ++neighbNeighbIt) {
					const Edge& neighbNeighbEdge = *neighbNeighbIt;
					const Node* neighbNeighb = neighbNeighbEdge.getToNode();
					if (neighbNeighb == seed1GeneNode) // don't count when seed1GeneNode is "indirectly connected" to itself via neighb
						continue;
					const Gene* neighbNeighbGene = neighbNeighb->getGene();
					const ConstLocusSet* neighbNeighbGeneSeeds2Loci = seeds2->getLoci(neighbNeighbGene);

					const double neighbNeighbWgt = neighbNeighbEdge.getWeight() * (neighbNeighbGene->getWeight());

					if (neighbNeighbGeneSeeds2Loci != NULL && // found an indirect seeds1-seeds2 interaction (via neighb)
							(_include_cseed_cseed_OR_seed_seed_connections_in_cross || ((!seeds2->inLoci(seed1Gene)) && (!seeds1->inLoci(neighbNeighbGene))))) { // option --allow-cseed-cseed-in-cross relaxes this constraint
						// ASSUME that Edges are multiplicative
						double indirWgt = wgt * neighbNeighbWgt;

						for (ConstLocusSet::const_iterator i = seed1GeneSeeds1Loci->begin(); i != seed1GeneSeeds1Loci->end(); ++i) {
							const Locus* loc1 = *i;
							for (ConstLocusSet::const_iterator j = neighbNeighbGeneSeeds2Loci->begin(); j != neighbNeighbGeneSeeds2Loci->end(); ++j) {
								const Locus* loc2 = *j;
								double useIndirWgt = indirWgt * (loc1->getWeight() * loc2->getWeight());
								/* Indirect (seeds1-seeds2) cross-connectivity */

								/* NOTE: In the simplest case, gene_n_indirect_connections tallies total # of
								 * indirect connections == number of paths such that: seedGene <---> CI <---> otherSeed [summed over all other seeds]
								 */
								IndirectEdge* e = ensureEdge<IndirectEdge>(seed1GeneNode, neighbNeighb, useIndirWgt, indirectEdges);
								if (e != NULL) e->addIntermediate(neighb);
								updateNetStats(seed1GeneNode, neighbNeighb, loc1, loc2, useIndirWgt, indirectCrossLocEdgesAggregator, indirectCrossGeneEdgesAggregator, e, &crossStats->indirect_cross);

								/* CI connectivity */
								// PER CI GENE
								LocPairStatisticAggregator* neighbGeneAgg = ensureAggregator(neighbGene, CIagg);
								neighbGeneAgg->addStat(LocPair(loc1,loc2), useIndirWgt);
							}
						}
					}
				}
			}
		}
	}

	crossStats->self_cross.agg_seed_n_connections = sumAndDeleteAggregators(selfCrossLocEdgesAggregator, crossStats->self_cross.seed_n_connections);
	crossStats->direct_cross.agg_seed_n_connections = sumAndDeleteAggregators(directCrossLocEdgesAggregator, crossStats->direct_cross.seed_n_connections);
	crossStats->indirect_cross.agg_seed_n_connections = sumAndDeleteAggregators(indirectCrossLocEdgesAggregator, crossStats->indirect_cross.seed_n_connections);

	crossStats->self_cross.agg_gene_n_connections = sumAndDeleteAggregators(selfCrossGeneEdgesAggregator, crossStats->self_cross.gene_n_connections);
	crossStats->direct_cross.agg_gene_n_connections = sumAndDeleteAggregators(directCrossGeneEdgesAggregator, crossStats->direct_cross.gene_n_connections);
	crossStats->indirect_cross.agg_gene_n_connections = sumAndDeleteAggregators(indirectCrossGeneEdgesAggregator, crossStats->indirect_cross.gene_n_connections);

	sumAndDeleteAggregators(CIagg, crossStats->gene_n_interactor_count);

	list<const Group*> seeds;
	seeds.push_back(seeds1);
	seeds.push_back(seeds2);
	if (netDetails) {
		printEdges(crossStats->self_cross, netDetails, seeds, "");
		printEdges(crossStats->direct_cross, netDetails, seeds, "");
		printEdges(crossStats->indirect_cross, netDetails, seeds, "");
	}

	deleteEdgeMap(selfEdges);
	deleteEdgeMap(directEdges);
	deleteEdgeMap(indirectEdges);
}

void NetFunc::Network::addBasicNetStats(NetStatsOutput& outStats, const BasicNetStats& netStats, string prefix) const {
	if (addGenicScores()) {
		addDataVals(netStats.seed_n_connections, prefix + "_score" + "_seed", *outStats._geneAndSeedStats);
		addDataVals(netStats.gene_n_connections, prefix + "_score" + "_gene", *outStats._geneAndSeedStats);
	}
	//int numSeedsWithStat = groupStats->n_seeds;
	int numSeedsWithStat = netStats.seed_n_connections.size();

	(*outStats._summaryStats)[prefix + "_edges"] = netStats.agg_seed_n_connections;
	//(*outStats._summaryStats)[prefix + "_genes"] = netStats.genes.size();
	(*outStats._summaryStats)[prefix + "_loci"] = numSeedsWithStat;
	(*outStats._summaryStats)[prefix + "_connectivity"] = numSeedsWithStat == 0 ? 0 : (*outStats._summaryStats)[prefix + "_edges"] / numSeedsWithStat;
}

void NetFunc::Network::addSingleSeedGroupStats(NetStatsOutput& stats, const SingleSeedGroupStats* groupStats, string groupPrefix) const {
	(*stats._summaryStats)[groupPrefix + "n_loci"] = groupStats->n_seeds;
	(*stats._summaryStats)[groupPrefix + "n_genes"] = groupStats->n_seed_genes;

	// --- General connectivity ---
	if (_calc_general_connectivity)
		addBasicNetStats(stats, groupStats->general, groupPrefix + "general");

	// --- Direct network connectivity ---
	addBasicNetStats(stats, groupStats->direct, groupPrefix + "direct");

	// --- Indirect network seed gene connectivity ---
	if (_calc_indirect_connectivity)
		addBasicNetStats(stats, groupStats->indirect, groupPrefix + "indirect");

	// ---- Common interactors ----
	if (_calc_common_interactors) {
		if (addGenicScores())
			addDataVals(groupStats->gene_n_interactor_count, groupPrefix + "gene_" + "interactor", *stats._geneAndSeedStats);

		int numCIgenes = groupStats->gene_n_interactor_count.size();
		(*stats._summaryStats)[groupPrefix + "interactor_nodes"] = numCIgenes;
	}
}

void NetFunc::Network::addCrossSeedGroupStats(NetStatsOutput& stats, const CrossSeedGroupStats* crossStats, string crossPrefix, string prefix1, string prefix2) const {
	/* Calculate some key delta scores
	 */
	for (std::list<std::string>::const_iterator statIt = DELTA_STATS.begin(); statIt != DELTA_STATS.end(); ++statIt) {
		const std::string& statName = *statIt;
		// TODO : consider A-B, vs. using A/(A+B), vs. using (A-B)/(A+B) :
		NameToVal::KeyToVal::const_iterator find1It = stats._summaryStats->find(prefix1 + statName);
		NameToVal::KeyToVal::const_iterator find2It = stats._summaryStats->find(prefix2 + statName);
		if (find1It != stats._summaryStats->end() && find2It != stats._summaryStats->end())
			(*stats._summaryStats)["delta-" + crossPrefix + statName] = find1It->second - find2It->second;
	}

	addBasicNetStats(stats, crossStats->self_cross, "cross-" + crossPrefix + "self");

	addBasicNetStats(stats, crossStats->direct_cross, "cross-" + crossPrefix + "direct");

	if (_calc_indirect_connectivity)
		addBasicNetStats(stats, crossStats->indirect_cross, "cross-" + crossPrefix + "indirect");

	// ---- Common interactors ----
	if (_calc_common_interactors) {
		if (addGenicScores())
			addDataVals(crossStats->gene_n_interactor_count, "cross-" + crossPrefix + "gene_" + "interactor", *stats._geneAndSeedStats);

		int numCIgenes = crossStats->gene_n_interactor_count.size();
		(*stats._summaryStats)["cross-" + crossPrefix + "interactor_nodes"] = numCIgenes;
	}
}

const std::string NetFunc::Network::DELTA_STATS_ARRAY[] = {
		"general_edges",
		"general_loci",
		"general_connectivity",
		"direct_edges",
		"direct_loci",
		"direct_connectivity",
		"indirect_edges",
		"indirect_loci",
		"indirect_connectivity",
		"interactor_nodes"
};
std::list<std::string> NetFunc::Network::DELTA_STATS = arrayToList(NetFunc::Network::DELTA_STATS_ARRAY);

NetFunc::NetStatsOutput* NetFunc::Network::netstats_full(NetDetails* netDetails) const {
	// 1. Primary seed list (always)
	//  and if optionally a cseed list is given:
	// 2. C-seed list
	// 3. P-C delta stats (for each non-genic(?) measure, are seeds > cseeds )
	// 4. P-C connectivity (do seeds and cseeds have more (in)direct connections than expected?)
	NetStatsOutput* calcStats = new NetStatsOutput();

	//
	// --- Primary seed list ---
	//
	std::string seedPrefix = _seeds->getLabel() + "_";
	SingleSeedGroupStats seedsStats;
	netstats_singleSeedGroup(_seeds, netDetails, &seedsStats);
	addSingleSeedGroupStats(*calcStats, &seedsStats, seedPrefix);

	//
	// --- Optionally, second comparator seed list ---
	//
	if (has_cseed_list()) {
		std::string cseedPrefix = _cseeds->getLabel() + "_";
		if (!_fix_cseed_genes_in_node_perms) {
			SingleSeedGroupStats cseedsStats;
			netstats_singleSeedGroup(_cseeds, netDetails, &cseedsStats);
			addSingleSeedGroupStats(*calcStats, &cseedsStats, cseedPrefix);
		}

		/* Finally, calculate some measures of relatedness BETWEEN the seed and cseed lists. Specifically:
		 * 1. Do these have more direct/indirect connections than expected by chance?
		 * 2. What are the common interactor genes that connect them?
		 */
		CrossSeedGroupStats crossSeedCseedStats;
		netstats_crossSeedGroups(_seeds, _cseeds, netDetails, &crossSeedCseedStats);
		std::string seedCseedPrefix = _seeds->getLabel() + "-" + _cseeds->getLabel() + "-";
		addCrossSeedGroupStats(*calcStats, &crossSeedCseedStats, seedCseedPrefix, seedPrefix, cseedPrefix);

		CrossSeedGroupStats crossCseedSeedStats;
		netstats_crossSeedGroups(_cseeds, _seeds, netDetails, &crossCseedSeedStats);
		std::string cseedSeedPrefix = _cseeds->getLabel() + "-" + _seeds->getLabel() + "-";
		addCrossSeedGroupStats(*calcStats, &crossCseedSeedStats, cseedSeedPrefix, cseedPrefix, seedPrefix);
	}

	return calcStats;
}

void NetFunc::Network::random_draw(std::vector<int>& a) {
	// Generate a random permutation of 0 to n-1 where n is a.size(),
	// using Fisher-Yates shuffle, simultaneously initializing from
	// 0..n-1 and shuffling

	const int n = a.size();
	for (int i = 0; i < n; i++)
		a[i] = i;

	int tmp;
	for (int i = n; i > 1; i--) {
		int j = CRandom::rand(i);
		tmp = a[i - 1];
		a[i - 1] = a[j];
		a[j] = tmp;
	}
}

NetFunc::Network::NodeToBinStats* NetFunc::Network::seed_bin_stats() {
	NodeToBinStats* stats = new NodeToBinStats();

	std::map<int, int> bincounts;
	std::map<int, int> seedcounts;
	std::map<int, std::string> binlabels;

	std::map<const Node*, int> node2bin;
	int binInd = 0;
	for (std::list<NodeBin>::const_iterator jj = _nodePermBins->bins().begin(); jj != _nodePermBins->bins().end(); ++jj) {
		const NodeVec& d = jj->nodes;
		const std::string binName = jj->label;
		//      const int n = d.size();

		for (NodeVec::const_iterator kk = d.begin(); kk != d.end(); ++kk) {
			const Node* node = *kk;
			const Gene* nodeGene = node->getGene();
			// for this, we don't care if seed or cseed, so just pool both
			// (also fine if a gene on both lists, we only need to display this
			// summary output once in any case)

			if (_seeds->inLoci(nodeGene) || _cseeds->inLoci(nodeGene)) {
				// this (c)seed node belongs to this bin:
				node2bin[node] = binInd;

				// and record that this bin has one more seed
				seedcounts[binInd]++;
			}

			// and denominator for this bin
			bincounts[binInd]++;

			binlabels[binInd] = binName;
		}

		binInd++;
	}

	NodeToQuantAttribute* medianNeighbDegrees = aggregateNeighborDegrees<medianOf>();
	NodeToQuantAttribute* sumNeighbDegrees = aggregateNeighborDegrees<sumOf>();

	// -- compile into the output that we want to display --
	for (std::map<const Node*, int>::const_iterator ll = node2bin.begin(); ll != node2bin.end(); ++ll) {
		const Node* nd = ll->first;
		const std::string& seedGeneName = nd->getGene()->getLabel();
		// this node belongs to bin
		const int binInd = ll->second;

		SingleNodeBinStats sbs;
		sbs.binLabel = "[" + binlabels[binInd] + "]";
		sbs.numTotalNodes = bincounts[binInd];
		sbs.numSeedNodes = seedcounts[binInd];
		sbs.degree = nd->numNeighbors();
		sbs.medianNeighborDegree = (*medianNeighbDegrees)[nd];
		sbs.sumNeighborDegree = (*sumNeighbDegrees)[nd];

		(*stats)[seedGeneName] = sbs;
	}

	delete medianNeighbDegrees;

	return stats;
}

Data::Matrix<double> NetFunc::Network::Influence_Matrix(const double gamma) const {
	Data::Matrix<double> L = Laplacian();
	for (int i = 0; i < L.dim1(); i++)
		L(i, i) += gamma;
	return Statistics::inverse(L);
}

std::vector<std::string> NetFunc::Network::node_labels() const {
	std::vector<std::string> l(_nodes.size());
	int i = 0;

	for (Network::NodeVec::const_iterator ii = _nodes.begin(); ii != _nodes.end(); ++ii)
		l[i++] = (*ii)->getGene()->getLabel();

	return l;
}

//http://compprog.wordpress.com/2008/01/17/finding-all-paths-of-minimum-length-to-a-node-using-dijkstras-algorithm/
//http://rosettacode.org/wiki/Dijkstra%27s_algorithm#C.2B.2B

void NetFunc::Network::DijkstraComputePaths(const Node* source,
					    std::vector<double>& min_distance,
					    std::vector<ConstNodeVec>& previous) const 
{


  const double max_weight = std::numeric_limits<double>::infinity();
  
  // number of nodes
  const int n = _nodes.size();
  
  // clear what will be output
  min_distance.clear();
  min_distance.resize(n, max_weight);
  min_distance[source->getNodeInd()] = 0; // using node's numeric (0..(n-1) identity)
  
  previous.clear();
  previous.resize(n); // i.e. a 'NULL' size 0 vector for each node initially
  
  //  std::cout << "source = " << source->getGene()->getLabel() << " " << source->getNodeInd() << "\n";
  
  typedef std::pair<double, const Node*> DistNode;
  std::set<DistNode> vertex_queue;
  vertex_queue.insert(DistNode(min_distance[source->getNodeInd()], source));
  
  while (!vertex_queue.empty()) 
    {
      double dist = vertex_queue.begin()->first;
      const Node* u = vertex_queue.begin()->second;
      vertex_queue.erase(vertex_queue.begin());
      
      //std::cout << "considering u = " << u->getGene()->getLabel() << "\n";
    
      // Visit each edge exiting u
      for (Node::Neighbors::const_iterator neighbIt = u->neighbBegin(); neighbIt != u->neighbEnd(); ++neighbIt) 
	{
	  const Node* v    = neighbIt->getToNode();
	  const int   vidx = v->getNodeInd();
	  double weight    = neighbIt->getWeight();
	  
	  //std::cout << "adding wgt " << neighbIt->getWeight() << " for " << u->getGene()->getLabel() << "-" << v->getGene()->getLabel() << "\n";
	  
	  double distance_through_u = dist + weight;
	  
	  // change to make a set of shortest paths
	  if (distance_through_u < min_distance[ vidx ] ) 
	    {
	      vertex_queue.erase(DistNode(min_distance[ vidx ], v));
	      min_distance[ vidx ] = distance_through_u;
	      previous[ vidx ].clear();
	      previous[ vidx ].push_back(u);
	      vertex_queue.insert( DistNode(min_distance[ vidx ], v ) );
	    }
	  else if (distance_through_u == min_distance[ vidx ] ) 
	    { // equally short path?
	      // add another shortest path
	      previous[ vidx ].push_back(u);
	    }
	}
    }
}

std::vector<NetFunc::Network::ConstNodeList> NetFunc::Network::DijkstraGetShortestPaths
(const Node* vertex, const std::vector<ConstNodeVec>* previous) const {
  std::vector<ConstNodeList> paths;
  DijkstraGetShortestPathsHelper(vertex, ConstNodeList(), &paths, previous);
  return paths;
}

void NetFunc::Network::DijkstraGetShortestPathsHelper(const Node* vertex,
						      ConstNodeList path, std::vector<ConstNodeList>* paths,
						      const std::vector<ConstNodeVec>* previous) const {
  
  path.push_back(vertex);
  const int v = vertex->getNodeInd();
  const ConstNodeVec& prev = (*previous)[v];
  const int np = prev.size();
  
  if (np == 0) {
    paths->push_back(path);
    return;
  }
  
  for (int i = 0; i < np; i++)
    DijkstraGetShortestPathsHelper(prev[i], path, paths, previous);
}


std::map<NetFunc::Node*,double> NetFunc::Network::shortestPathLengths( const Node * a ) const
{
  std::map<Node*,double> d;

  // for each node, shortest distance (weighted)
  std::vector<double> min_distance;

  // store all possible shortest paths
  std::vector<ConstNodeVec> previous;
  
  DijkstraComputePaths(a, min_distance, previous);
  
  const int n = _nodes.size();
  
  for (int i = 0; i < n; i++) 
    {
      Node * node = _nodes[ i ];  
      d[ node ] = min_distance[ i ];
    }
  return d;
}

NetFunc::paths_t NetFunc::Network::Dijkstra( const Node* a ) const 
{
  
  paths_t paths;
  
  // for each node, shortest distance (weighted)
  std::vector<double> min_distance;

  // store all possible shortest paths
  std::vector<ConstNodeVec> previous;
  
  DijkstraComputePaths(a, min_distance, previous);
    
  // this is just output / example of how to use function...
  if ( false )
    {

      const int n = _nodes.size();
      
      for (int i = 0; i < n; i++) 
	{
	  std::vector<NetFunc::Network::ConstNodeList> paths = DijkstraGetShortestPaths(_nodes[i], &previous);
	  
	  std::cout << "distance from " << a->getGene()->getLabel() << " to " << _nodes[i]->getGene()->getLabel() << "\t" << min_distance[i] << "\n";
	  
	  for (int p = 0; p < paths.size(); p++) 
	    {
	      std::cout << "\tpath" << p << "\t";
	      for (ConstNodeList::const_iterator ii = paths[p].begin(); ii != paths[p].end(); ++ii)
		std::cout << " " << (*ii)->getGene()->getLabel();
	      
	      std::cout << "\n";
	    }
	}
    }

  return paths;
}


const NetFunc::Network::EquivNodesList* NetFunc::Network::calc_connected_components() {
	if (_components != NULL)
		delete _components;

	DisjointSet<const Node*> V;

	for (NodeVec::const_iterator ii = _nodes.begin(); ii != _nodes.end(); ++ii) {
		const Node* nd = *ii;
		for (Node::Neighbors::const_iterator jj = nd->neighbBegin(); jj != nd->neighbEnd(); ++jj) {
			if (V.findSetRep(nd) != V.findSetRep(jj->getToNode()))
				V.setUnion(nd, jj->getToNode());
		}
	}
	_components = V.getCurrentEquivSets();
	return _components;
}

void NetFunc::Network::create_subnetwork(double delta) {
	for (NodeVec::const_iterator ii = _nodes.begin(); ii != _nodes.end(); ++ii) {
		Node* node = *ii;
		const int n1 = node->getNodeInd();
		for (Node::Neighbors::const_iterator neighbIt = node->neighbBegin(); neighbIt != node->neighbEnd(); ++neighbIt) {
			const Edge& e = *neighbIt;
			const Node* neighb = e.getToNode();

			if (e.getWeight() < delta)
				removeDirectedEdge(n1, neighb->getNodeInd());
		}
	}
}

void NetFunc::Network::create_network_from_matrix(const Data::Matrix<double>& m, const double thr, const bool greater_than) {
	// First, clear all connections
	removeAllEdges();

	// We expect the matrix to be in the same order as nodes:
	const int nrow = m.dim1();
	const int ncol = m.dim2();

	for (int c = 0; c < ncol; c++) {
		for (int r = 0; r < nrow; r++) {
			if ((greater_than && m(r, c) >= thr) || (!greater_than && m(r, c) < thr))
				addUndirectedEdge(r, c, m(r, c));
		}
	}
}

void NetFunc::Network::remove_edges_based_on_matrix_vals(const Data::Matrix<double>& m, const double thr, bool remove_if_less_than) {
	// We expect the matrix to be in the same order as nodes:
	for (NodeVec::const_iterator ii = _nodes.begin(); ii != _nodes.end(); ++ii) {
		Node* node = *ii;
		const int n1 = node->getNodeInd();
		for (Node::Neighbors::const_iterator neighbIt = node->neighbBegin(); neighbIt != node->neighbEnd(); ++neighbIt) {
			const Edge& e = *neighbIt;
			const Node* neighb = e.getToNode();
			const int n2 = neighb->getNodeInd();

			if ((remove_if_less_than && m(n1, n2) <= thr) || (!remove_if_less_than && m(n1, n2) > thr))
				removeDirectedEdge(n1, n2);
		}
	}
}

// For HotNet, remove edges with score below threshold thr [score == the sum (or max) of the two genic scores times value in the matrix]:
void NetFunc::Network::remove_edges_from_matrix_seed_score_enhanced(const Data::Matrix<double>& m, const double thr) {
	// We expect the matrix to be in the same order as nodes:
	for (NodeVec::const_iterator ii = _nodes.begin(); ii != _nodes.end(); ++ii) {
		Node* node = *ii;
		const int n1 = node->getNodeInd();
		for (Node::Neighbors::const_iterator neighbIt = node->neighbBegin(); neighbIt != node->neighbEnd(); ++neighbIt) {
			const Edge& e = *neighbIt;
			const Node* neighb = e.getToNode();
			const int n2 = neighb->getNodeInd();
			const double u = m(n1, n2);

			// will be 0 if not a seed
			// otherwise, assume -2*log(p)
			const double e1 = _seeds->getSumLociWeights(node->getGene());
			const double e2 = _seeds->getSumLociWeights(neighb->getGene());

			// max:
			//const double w = e1 > e2 ? u * e1 : u * e2 ;

			// sum:
			const double w = u * (e1 + e2);

			if (w < thr)
				removeDirectedEdge(n1, n2);
		}
	}
}

NetFunc::Network::ConnComp NetFunc::Network::get_connected_components_of_size(int s, int t) {
	if (t == -1)
		t = s;

	ConnComp r;
	if (_components == NULL)
		calc_connected_components();

	for (Network::EquivNodesList::const_iterator ii = _components->begin(); ii != _components->end(); ++ii) {
		const Network::EquivNodes& cc = ii->second;
		if (cc.size() >= s && cc.size() <= t) {
			std::set<std::string> rr;

			for (Network::EquivNodes::const_iterator jj = cc.begin(); jj != cc.end(); ++jj) {
				const Node* node = *jj;
				// if ( node == NULL ) std::cout << "node is null\n";
				// if ( node->gene == NULL ) std::cout << "gene is null\n";
				// std::cout << node->gene->getLabel() << " is label\n";
				rr.insert(node->getGene()->getLabel());
			}
			r.push_back(rr);
		}
	}

	return r;
}
