#ifndef _PSEQ_DISJOINT_SET__
#define _PSEQ_DISJOINT_SET__

// This class from the SPRINT package (http://www.protonet.cs.huji.ac.il/sprint/)
// Copyright 2010 Menachem Fromer and Chen Yanover

#include <set>
#include <map>
using namespace std;

/* Data Structures for Disjoint Sets. */
template <class Item>
class DisjointSet {
public:

	DisjointSet();

	~DisjointSet();

	void setUnion(const Item& x, const Item& y);

	//Returns x if x does not have any set it's in:
	Item findSetRep(const Item& x) const;

	typedef set<Item> EquivSet;
	typedef map<Item, EquivSet> EquivSetList;
	EquivSetList* getCurrentEquivSets() const;

	inline bool empty() const;

private:
	class ItemNode {
	public:

		ItemNode(const Item& item);

		~ItemNode();

		Item _item;
		ItemNode* _parent;
		unsigned int _rank;
	};

	typedef map<Item,ItemNode*> NodeMap;
	NodeMap* _nodeMap;

	ItemNode* findSet(ItemNode* x) const;

	ItemNode* findSetWithPathCompression(ItemNode* x);

	void setUnion(ItemNode* x, ItemNode* y);

	void link(ItemNode* x, ItemNode* y);

	ItemNode* ensureNode(const Item& item);
};


//Implementation:
template <class Item>
DisjointSet<Item>::DisjointSet()
: _nodeMap(new NodeMap()) {}

template <class Item>
DisjointSet<Item>::~DisjointSet() {
	for (typename NodeMap::const_iterator nodeIt = _nodeMap->begin();
			nodeIt != _nodeMap->end(); ++nodeIt) {
		delete nodeIt->second;
	}
	delete _nodeMap;
}

template <class Item>
void DisjointSet<Item>::setUnion(const Item& x, const Item& y) {
	ItemNode* xNode = this->ensureNode(x);
	ItemNode* yNode = this->ensureNode(y);
	setUnion(xNode, yNode);
}

template <class Item>
Item DisjointSet<Item>::findSetRep(const Item& x) const {
	typename NodeMap::iterator findItemIt = _nodeMap->find(x);
	if (findItemIt == _nodeMap->end())
		return x;

	ItemNode* xNode = findItemIt->second;
	return this->findSet(xNode)->_item;
}

template <class Item>
typename DisjointSet<Item>::EquivSetList*
DisjointSet<Item>::getCurrentEquivSets() const {

	typedef map<ItemNode*, EquivSet> RootToSet;
	RootToSet* rootToSetMap = new RootToSet();
	for (typename NodeMap::const_iterator nodeIt = _nodeMap->begin();
			nodeIt != _nodeMap->end(); ++nodeIt) {
		const Item& x = nodeIt->first;
		ItemNode* xNode = nodeIt->second;
		(*rootToSetMap)[ findSet(xNode) ].insert(x);
	}

	EquivSetList* setList = new EquivSetList();
	for (typename RootToSet::const_iterator rootIt = rootToSetMap->begin();
			rootIt != rootToSetMap->end(); ++rootIt) {
		const Item& rootItem = rootIt->first->_item;
		(*setList)[rootItem] = rootIt->second;
	}
	delete rootToSetMap;
	return setList;
}

template <class Item>
inline bool DisjointSet<Item>::empty() const {
	return _nodeMap->empty();
}

template <class Item>
typename DisjointSet<Item>::ItemNode*
DisjointSet<Item>::ensureNode(const Item& item) {
	typename NodeMap::iterator findItemIt = _nodeMap->find(item);
	if (findItemIt == _nodeMap->end())
		findItemIt = _nodeMap->insert(make_pair(item, new ItemNode(item))).first;

	return findItemIt->second;
}

template <class Item>
typename DisjointSet<Item>::ItemNode*
DisjointSet<Item>::findSet(ItemNode* x) const {

	//Find x's root Node:
	ItemNode* curNode = x;
	while (curNode->_parent != curNode)
		curNode = curNode->_parent;

	return curNode;
}

template <class Item>
typename DisjointSet<Item>::ItemNode*
DisjointSet<Item>::findSetWithPathCompression(ItemNode* x) {

	ItemNode* root = this->findSet(x);

	//Perform path compression:
	ItemNode* curNode = x;
	while (curNode != root) {
		ItemNode* next = curNode->_parent;
		curNode->_parent = root;
		curNode = next;
	}

	return root;
}

template <class Item>
void DisjointSet<Item>::setUnion(ItemNode* x, ItemNode* y) {
	link(findSetWithPathCompression(x), findSetWithPathCompression(y));
}

template <class Item>
void DisjointSet<Item>::link(ItemNode* x, ItemNode* y) {
	if (x == y)
		return;

	//Union by rank:
	if (x->_rank > y->_rank) {
		y->_parent = x;
	}
	else { // x->_rank <= y->_rank
		x->_parent = y;
		if (x->_rank == y->_rank)
			++(y->_rank);
	}
}

//For ItemNode:
template <class Item>
DisjointSet<Item>::ItemNode::ItemNode(const Item& item)
: _item(item), _parent(this), _rank(0) {}

template <class Item>
DisjointSet<Item>::ItemNode::~ItemNode() {}

#endif
